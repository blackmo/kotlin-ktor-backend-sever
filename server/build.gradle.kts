import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

plugins {
    java
    kotlin("jvm")
    application
    id("com.github.johnrengelman.shadow") version ("5.2.0")
}

group = "com.vhl"
version = "1.0-SNAPSHOT"


val ktor_version = "1.3.1"
val kodein_verison = "6.5.4"
val exposedVersion = "0.23.1"
val csv_reader_version = "0.7.3"

repositories {
    mavenCentral()
    jcenter()
    maven("https://jitpack.io")
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))

    //-- local jar files
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    //ktor
    implementation("io.ktor:ktor-server-netty:$ktor_version")
    implementation("io.ktor:ktor:$ktor_version")
    implementation("io.ktor:ktor-websockets:$ktor_version")
    implementation("io.ktor:ktor-gson:$ktor_version")
    implementation("io.ktor:ktor-serialization:$ktor_version")

    //logging
    implementation("io.github.microutils:kotlin-logging:1.6.24")
    implementation("ch.qos.logback:logback-classic:1.2.3")

    // token verification
    implementation("io.ktor:ktor-auth-jwt:$ktor_version")

    //json handlers
    implementation("com.beust:klaxon:5.0.1")

    //encryption
    implementation("com.github.simbiose:Encryption:2.0.1")
    implementation("commons-codec:commons-codec:1.14")

    //di
    implementation("org.kodein.di","kodein-di-generic-jvm",kodein_verison)

    //redis
    implementation("io.lettuce:lettuce-core:5.2.1.RELEASE")

    //database
    implementation("org.jetbrains.exposed", "exposed-core", exposedVersion)
    implementation("org.jetbrains.exposed", "exposed-dao", exposedVersion)
    implementation("org.jetbrains.exposed", "exposed-jdbc", exposedVersion)
    implementation("org.jetbrains.exposed", "exposed-java-time", exposedVersion)

    //csv reader
    implementation("com.github.doyaaaaaken:kotlin-csv-jvm:$csv_reader_version")

    implementation("com.zaxxer:HikariCP:3.4.1")
    implementation ("mysql:mysql-connector-java:8.0.11")

    //-- jasper reports
    implementation("net.sf.jasperreports", "jasperreports", "6.4.0") {
        exclude("org.olap4j", "olap4j")
    }
    implementation("com.lowagie", "itext", "2.1.7")

    // project
    implementation( project(":sms-caster-commons"))

    //test
    testCompile("junit", "junit", "4.12")
    testCompile("io.ktor:ktor-server-test-host:$ktor_version")
}

application {
    mainClassName = "com.vhl.sms.caster.MainAppKt"
}

tasks.withType<ShadowJar> {
    baseName = "school-db-server"
    classifier = null
    version = null
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }

    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
}