package com.vhl.sms.caster.features

import com.vhl.sms.caster.KodeinInstance
import com.vhl.sms.caster.commons.errors.MissingRequiredInputsError
import com.vhl.sms.caster.commons.model.enums.Privileges
import com.vhl.sms.caster.constants.ModuleTags
import com.vhl.sms.caster.service.TokenSessionService
import com.vhl.sms.caster.storage.redis.Redis
import com.vhl.sms.caster.utils.validateToken
import getTokenModel
import io.ktor.application.*
import io.ktor.routing.*
import io.ktor.util.AttributeKey
import io.ktor.util.pipeline.PipelinePhase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.kodein.di.generic.instance

class TokenValidation(
    private val configuration: Configuration,
    private val redis: Redis? = null
) {
    class Configuration {
        var tokenSessionService: TokenSessionService? = null
        var retrievePrivileges: Boolean = false
    }

    suspend fun addAttribute(call: ApplicationCall) = withContext(Dispatchers.IO) {
        val tokenModel = call.request.getTokenModel()
            ?.let {
                val privilege = "privilege::${it.id}"
                redis?.async?.get(privilege)?.get()
            }

        call.attributes.put(InjectedAttributeKeys.USER_PRIVILEGE, tokenModel ?: Privileges.ADMIN.name)
    }

    fun interceptRoute(pipeline: ApplicationCallPipeline) {
        pipeline.insertPhaseBefore(ApplicationCallPipeline.Features, TokenPhase)
        pipeline.intercept(TokenPhase) {

            if (configuration.retrievePrivileges) {
                addAttribute(call)
            }

            configuration.tokenSessionService?.run {
                call.validateToken(this) {
                    finish()
                }
            } ?: throw MissingRequiredInputsError(
                "Token Validation service is not initialized",
                "FEATURE_TOKEN_VALIDATION"
            )
        }
    }

    companion object Feature : ApplicationFeature<ApplicationCallPipeline, Configuration, TokenValidation> {

        val kodein = KodeinInstance.kodein ?: throw IllegalStateException("Kodein DI not initialized")
        val TokenPhase: PipelinePhase = PipelinePhase("TokenPhase")
        override val key = AttributeKey<TokenValidation>("TokenValidationFeature")


        override fun install(pipeline: ApplicationCallPipeline, configure: Configuration.() -> Unit): TokenValidation {

            val configuration = Configuration().apply(configure)

            return when {
                /**
                 *  creates a features that every call there is interception
                 */
                configuration.retrievePrivileges -> {
                    val redis: Redis by kodein.instance<Redis>(ModuleTags.REDIS_DB1)
                    val feature = TokenValidation(configuration, redis)

//                    pipeline.intercept(ApplicationCallPipeline.Call) {
//                        feature.addAttribute(this)
//                    }

                    return feature
                }
                /**
                 * creates default features
                 */
                else -> TokenValidation(configuration)
            }
        }
    }
}

fun Route.validateToken(
    build: Route.() -> Unit
): Route {
    val route = createChild(object : RouteSelector(1.0) {
        override fun evaluate(context: RoutingResolveContext, segmentIndex: Int): RouteSelectorEvaluation =
            RouteSelectorEvaluation.Constant
    })

    application.feature(TokenValidation).interceptRoute(route)
    route.build()
    return route
}

class InjectedAttributeKeys {
    companion object {
        val TIME_STAMP = AttributeKey<Long>("time-stamp")
        val USER_PRIVILEGE = AttributeKey<String>("user-privilege")
    }
}