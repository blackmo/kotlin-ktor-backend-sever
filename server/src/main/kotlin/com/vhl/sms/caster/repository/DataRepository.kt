package com.vhl.sms.caster.repository

interface DataRepository<T> {
    fun add(entity: T): T
    fun delete(entity: T)
    fun update(entity: T): T
    fun findOneById(id: Int): T?
    fun findAllAndPaginate(count: Int, offset: Int): List<T>
}

interface Repository<T, U> : DataRepository<T>

