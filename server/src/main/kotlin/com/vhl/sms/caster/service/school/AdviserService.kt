package com.vhl.sms.caster.service.school

import com.vhl.sms.caster.commons.errors.E_ALREADY_EXIST
import com.vhl.sms.caster.commons.errors.FailedInsertError
import com.vhl.sms.caster.commons.model.school.Adviser
import com.vhl.sms.caster.commons.payload.school.person.FPersonByName
import com.vhl.sms.caster.commons.response.Response
import com.vhl.sms.caster.commons.warnings.W_ALREADY_EXIST
import com.vhl.sms.caster.datasource.access.school.AdviserAccess
import com.vhl.sms.caster.datasource.access.school.PersonAccess
import com.vhl.sms.caster.repository.school.AdviserRepository
import com.vhl.sms.caster.service.ServiceBase
import com.vhl.sms.caster.utils.StatusResponse
import com.vhl.sms.caster.utils.handler.error.responseCatch

class AdviserService(
    private val adviserRepository: AdviserRepository,
    private val personService: PersonService
): ServiceBase<Adviser> {
    override fun add(
        entity: Adviser,
        createdBy: Int,
        response: (code: String, message: String, dataValue: Adviser?) -> Response<Adviser>
    ): Response<Adviser> {
        return adviserRepository.attemptSave(entity, createdBy)
    }

    override fun delete(
        entity: Adviser,
        response: (code: String, message: String, dataValue: Adviser?) -> Response<Adviser>
    ): Response<Adviser> {
        return responseCatch {
            adviserRepository.delete(AdviserAccess() from entity)
            response(StatusResponse.SUCCESS, "Successfully Deleted Adviser", null)
        }
    }

    override fun update(
        entity: Adviser,
        modifiedBy: Int,
        response: (code: String, message: String, dataValue: Adviser?) -> Response<Adviser>
    ): Response<Adviser> {
        return responseCatch {
            val adviserAccess = AdviserAccess().from(entity).apply { this.modifiedBy = modifiedBy }
            val data = adviserRepository.update(adviserAccess)
            response(StatusResponse.SUCCESS, "Successfully Updated Adviser", data.toAdviser())
        }
    }

    override fun findOneById(
        id: Int,
        response: (code: String, message: String, dataValue: Adviser?) -> Response<Adviser>
    ): Response<Adviser> {
        return responseCatch {
            val data = adviserRepository.findOneById(id)
            response(StatusResponse.SUCCESS, "Successfully Retrieved Adviser", data?.toAdviser())
        }
    }

    override fun findAllAngPaginate(
        count: Int,
        offset: Int,
        response: (code: String, message: String, dataValue: List<Adviser>?) -> Response<List<Adviser>>
    ): Response<List<Adviser>> {
        return responseCatch {
            val data = adviserRepository.findAllAndPaginate(count, offset)
                .map { it.toAdviser() }

            response(StatusResponse.SUCCESS, "Successfully Retrieved Adviser", data)
        }
    }

    fun findOneEager(
        id: Int,
        response: (code: String, message: String, dataValue: Adviser?) -> Response<Adviser>
    ): Response<Adviser> {
        return responseCatch {
            val data = adviserRepository.findOneEager(id)
            response(StatusResponse.SUCCESS, "Successfully Retrieved Adviser", data?.toAdviser())
        }
    }

    fun findOneEagerLike(
        payload: FPersonByName,
        limit: Boolean,
        response: (code: String, message: String, dataValue: List<Adviser>?) -> Response<List<Adviser>>
    ): Response<List<Adviser>> {
        return responseCatch {
            val data = adviserRepository.findByEagerLike(
                payload.lastName,
                payload.firstName,
                payload.middleName,
                limit,
                payload.count,
                payload.offset
            ).map { it.toAdviser() }

            response(StatusResponse.SUCCESS, "Successfully Retrieved Advisers", data)
        }
    }

    fun getAllAdviser(
        response: (code: String, message: String, dataValue: List<Adviser>?) -> Response<List<Adviser>>
    ): Response<List<Adviser>> {
        return responseCatch {
            val data = adviserRepository.findAllByEager()

            response(StatusResponse.SUCCESS, "Successfully Retrieved Advisers", data.map { it.toAdviser() })
        }
    }

    //-- private methods
    private fun AdviserRepository.attemptSave(adviser: Adviser, createdBy: Int): Response<Adviser> = responseCatch {
        val personAccess = PersonAccess().from(adviser.person).apply { this.createdBy = createdBy }
        val person = personService.addOrGet(personAccess) { code, message, dataValue ->
            when (code) {
                StatusResponse.SUCCESS -> Response(code, message, dataValue)
                E_ALREADY_EXIST -> Response(W_ALREADY_EXIST, "Student Data Already Exist", adviser.person)
                else -> throw FailedInsertError(message, "ST_SVC")
            }
        }

        val adviserInput = Adviser(0, person.data!!, adviser.licenseNumber)
        val registeredAdviser = add(AdviserAccess() from adviserInput).toAdviser()

        return Response(
            StatusResponse.SUCCESS,
            "Adviser Created || ${person.message}",
            registeredAdviser
        )
    }
}