package com.vhl.sms.caster.repository.school

import com.vhl.sms.caster.commons.constants.school.student.status.StudentStatus
import com.vhl.sms.caster.commons.errors.AlreadyExistError
import com.vhl.sms.caster.datasource.access.school.*
import com.vhl.sms.caster.exposed.ExposedSchema
import com.vhl.sms.caster.repository.DataRepository
import com.vhl.sms.caster.utils.formatter.notEmptyLike
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.UpdateBuilder
import org.jetbrains.exposed.sql.transactions.transaction

class StudentRepository(private val personRepository: PersonRepository):
    ExposedSchema<StudentAccess, StudentDao>(), DataRepository<StudentAccess> {

    companion object {
        private const val   ALIAS_PERSON = "person"
        private const val ALIAS_MOTHER = "mother"
        private const val ALIAS_FATHER = "father"
    }


    private val personTable = PersonDao.alias(ALIAS_PERSON)
    private val motherTable = PersonDao.alias(ALIAS_MOTHER)
    private val fatherTable = PersonDao.alias(ALIAS_FATHER)

    private val fieldSets = mutableListOf<Expression<*>>()
        .also {
            it.add(StudentDao.id)
            it.add(StudentDao.lrnID)
            it.add(StudentDao.status)
            it.add(StudentDao.createdAt)
            it.add(StudentDao.modifiedAt)
            it.addAll(personFieldWithAlias(personTable))
            it.addAll(personFieldWithAlias(motherTable))
            it.addAll(personFieldWithAlias(fatherTable))
        }

    private val studentFieldSets = mutableListOf<Expression<*>>()
        .also {
            it.add(StudentDao.lrnID)
            it.add(StudentDao.id)
            it.add(StudentDao.status)
            it.add(PersonDao.id)
            it.addAll(personFields())
        }

    override val entityName = "Student"
    override val originName = "ORIGIN_STUDENT"

    override fun add(entity: StudentAccess): StudentAccess = transaction {
        StudentDao.checkIfAlreadyExist {
            (StudentDao.lrnID eq entity.lrnID) or (StudentDao.id eq entity.id)
        }.run {
            if (this) throw AlreadyExistError("Student Already Exist with LRN-ID [${entity.lrnID}] ", originName)
        }
        StudentDao.insertOne(entity)
    }

    override fun delete(entity: StudentAccess) = transaction {
        StudentDao.deleteWhere { StudentDao.id eq entity.id }
        commit()
    }

    override fun update(entity: StudentAccess): StudentAccess = transaction {
        StudentDao.updateOne(entity) {
            (StudentDao.id eq entity.id) and  (StudentDao.lrnID eq entity.lrnID)
        }
    }

    override fun findOneById(id: Int): StudentAccess? = transaction {
        StudentDao
            .innerJoin(PersonDao, { person }, { PersonDao.id })
            .select {
                (StudentDao.id eq id)
            }.mapNotNull { it.joinWithPerson() }.firstOrNull()
    }

    fun findByLrnIdOrLastName(lrnID: String, lastName: String, status: String?) = transaction {
        var queryOp = findByEagerAction {
            (StudentDao.lrnID like lrnID.notEmptyLike()) or (PersonDao.lastName like lastName.notEmptyLike())
        }

        queryOp = when (status) {
            "", null -> queryOp
            else -> queryOp.andWhere { StudentDao.status eq status.toUpperCase() }
        }

        queryOp.mapNotNull { it.joinStudentWithParents() }
    }

    fun findByEager(lrnID: String, id: Int?): List<StudentAccess> = transaction {
        val query = findByEagerAction { StudentDao.lrnID like "$lrnID%" }
        val queryResult = id?.let { query.orWhere { StudentDao.id eq it } } ?: query

        queryResult.mapNotNull { it.joinStudentWithParents() }
    }

    fun updateStudentStatus(studentId: Int, status: StudentStatus) = transaction {
        StudentDao.updateOne({ StudentDao.id eq studentId }) {
            it[StudentDao.status] = status.name
        }
    }

    private fun findByEagerAction(where: SqlExpressionBuilder.() -> Op<Boolean>) =
        StudentDao
            .innerJoin(personTable, { person }, { personTable[PersonDao.id] })
            .leftJoin(motherTable, { StudentDao.mother }, { motherTable[PersonDao.id] })
            .leftJoin(fatherTable, { StudentDao.father }, { fatherTable[PersonDao.id] })
            .slice(fieldSets)
            .select(where)


    override fun findAllAndPaginate(count: Int, offset: Int): List<StudentAccess> = transaction {
       StudentDao.findAllAndPaginate(count, offset)
    }

    override fun UpdateBuilder<Any>.from(entity: StudentAccess, dao: StudentDao) {
        this[dao.id] = entity.id
        this[dao.lrnID] = entity.lrnID
        this[dao.mother] = entity.mother?.id
        this[dao.father] = entity.father?.id
        this[dao.createdBy] = entity.createdBy
        this[dao.modifiedBy] = entity.modifiedBy

        entity.person?.id?.let { this[dao.person] = it }
    }

    override fun ResultRow.toEntity(): StudentAccess {
        val studentAccess = StudentAccess()

        studentAccess.id = this[StudentDao.id]
        studentAccess.lrnID = this[StudentDao.lrnID]
        studentAccess.person = PersonAccess() itsId this[StudentDao.person]
        studentAccess.mother = parentNullOrHasId(this[StudentDao.mother])
        studentAccess.father = parentNullOrHasId(this[StudentDao.father])

        studentAccess.createdAt = this[StudentDao.createdAt]
        studentAccess.modifiedAt = this[StudentDao.modifiedAt]

        return studentAccess
    }

    private fun ResultRow.joinWithPerson(): StudentAccess {
        val studentAccess = StudentAccess()
        studentAccess.id = this[StudentDao.id]
        studentAccess.lrnID = this[StudentDao.lrnID]
        studentAccess.person = personRepository fromResultRow this
        studentAccess.mother = parentNullOrHasId(this[StudentDao.mother])
        studentAccess.father = parentNullOrHasId(this[StudentDao.father])
        studentAccess.status = this[StudentDao.status]

        studentAccess.createdAt = this[StudentDao.createdAt]
        studentAccess.modifiedAt = this[StudentDao.modifiedAt]

        return studentAccess
    }

    private fun ResultRow.joinStudentWithParents(): StudentAccess {
        val studentAccess = StudentAccess()
        studentAccess.id = this[StudentDao.id]
        studentAccess.lrnID = this[StudentDao.lrnID]
        studentAccess.status = this[StudentDao.status]

        val person = personRepository.createAccessFromAliasTable(this, personTable)
        val mother = personRepository.createAccessFromAliasTable(this, motherTable)
        val father = personRepository.createAccessFromAliasTable(this, fatherTable)

        studentAccess.person = person
        studentAccess.mother = mother
        studentAccess.father = father

        return studentAccess
    }

    infix fun studentSoftPersonFields(resultRow: ResultRow): StudentAccess {
        val student = StudentAccess()
        student.id = resultRow[StudentDao.id]
        student.lrnID = resultRow[StudentDao.lrnID]
        student.status = resultRow[StudentDao.status]
        student.person = personRepository fromResultRow resultRow

        return student
    }

    fun joinEagerStudentWithParents(resultRow: ResultRow) = resultRow.joinStudentWithParents()

    infix fun fromResultRow(resultRow: ResultRow): StudentAccess = resultRow.toEntity()

    private fun parentNullOrHasId(id: Int?): PersonAccess? = when {
        id != null -> PersonAccess() itsId id
        else -> null
    }
}
