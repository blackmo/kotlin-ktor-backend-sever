package com.vhl.sms.caster.handler

import com.vhl.sms.caster.router.SocketLifeStatus
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.WebSocketSession
import kotlinx.coroutines.delay
import java.util.concurrent.ConcurrentHashMap

class SocketSessionHandler {
    var checking: Boolean = false

    private val socketLifeStatus = ConcurrentHashMap<String, SocketLifeStatus>()
    private val userConnectedSockets = mutableMapOf<String, ConcurrentHashMap<String, WebSocketSession>>()

    fun addConnectedDeviceToUser(user: String, deviceId: String, socket: WebSocketSession) {
        val userDevices = userConnectedSockets[user] ?: ConcurrentHashMap()
        userDevices[deviceId] = socket
        socketLifeStatus[deviceId] = SocketLifeStatus(socketId = deviceId, user = user)
        userConnectedSockets[user] = userDevices
    }

    fun getUserConnectedDevice(user: String, deviceId: String) =
        userConnectedSockets[user]?.get(deviceId)

    fun getUserConnectedDeviceIds(user: String) = userConnectedSockets[user]?.keys()?.toList()

    fun getSocketLifeStatus(key: String) = socketLifeStatus[key]

    suspend fun sendCommand(user: String, deviceId: String, command: String) {
       userConnectedSockets[user]?.get(deviceId)?.send(Frame.Text(command))
    }

    suspend fun checkLiveSocket() {
        if (checking) return
        else checking = !checking

        while (checking) {
            delay(1500)
            processUserConnectedDevice()
        }
    }

    private suspend fun sendCommand(key: String, socket: WebSocketSession) {
        val status = socketLifeStatus[key]
        if (status?.commands?.isNotEmpty()!!) {
            repeat(status.commands.size) {
                val command = status.commands.removeAt(0)
                socket.send(Frame.Text(command))
            }
        }
    }

    private suspend fun processUserConnectedDevice() {
        userConnectedSockets.forEach { (user, sockets) ->
            runCatching {
                sockets.forEach {socketEntry ->
                    when {
                        !socketEntry.value.outgoing.isClosedForSend -> {
                            sendCommand(socketEntry.key, socketEntry.value)
                        }
                        else -> {
                            sockets.remove(socketEntry.key)
                            socketLifeStatus.remove(socketEntry.key)
                        }
                    }
                }
            }
        }
    }
}