package com.vhl.sms.caster.datasource.access.school

import com.vhl.sms.caster.commons.model.school.Adviser
import com.vhl.sms.caster.datasource.access.UserDoa
import com.vhl.sms.caster.exposed.Dao
import com.vhl.sms.caster.exposed.Domain
import java.time.LocalDateTime

object AdviserDao: Dao("adviser") {
    val person = integer("person_id").references(PersonDao.id).uniqueIndex()
    val licenseNumber = varchar("license_number", 20).nullable().uniqueIndex()

    val createdBy = integer("created_by").references(UserDoa.id).nullable()
    val modifiedBy = integer("modified_by").references(UserDoa.id).nullable()
}

class AdviserAccess: Domain {

    override var id: Int = 0
    var person: PersonAccess? = null
    var licenseNumber: String? = ""

    override var createdAt: LocalDateTime? = null
    override var modifiedAt: LocalDateTime? = null

    var createdBy: Int? = null
    var modifiedBy: Int? = null

    infix fun from(adviser: Adviser): AdviserAccess {
        id = adviser.id
        person = PersonAccess() from adviser.person
        licenseNumber = adviser.licenseNumber

        return this
    }

    infix fun itsID(id: Int): AdviserAccess {
        this.id = id
        return this
    }

    fun toAdviser(): Adviser {
        return Adviser(
            id,
            this.person?.toPerson() ?: throw IllegalArgumentException("Adviser persona no initialized"),
            licenseNumber

        )
    }
}