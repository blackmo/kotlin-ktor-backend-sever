package com.vhl.sms.caster.repository.school

import com.vhl.sms.caster.commons.errors.AlreadyExistError
import com.vhl.sms.caster.datasource.access.school.PersonAccess
import com.vhl.sms.caster.datasource.access.school.PersonDao
import com.vhl.sms.caster.exposed.ExposedSchema
import com.vhl.sms.caster.repository.DataRepository
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.UpdateBuilder
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.LocalDateTime

class PersonRepository:
    ExposedSchema<PersonAccess, PersonDao>(), DataRepository<PersonAccess> {

    override val entityName: String = "Person"
    override val originName: String = "ORIGIN_PERSON"

    override fun add(entity: PersonAccess): PersonAccess = transaction {
        PersonDao.checkIfAlreadyExist {
            (PersonDao.firstName eq entity.firstName) and (PersonDao.lastName eq entity.lastName)
        }.run {
            if (this) throw AlreadyExistError("Person Already Exist", originName)
        }
        PersonDao.insertOne(entity)
    }

    fun addOrGet(entity: PersonAccess): PersonAccess = transaction {
        PersonDao.select {
                (PersonDao.firstName eq entity.firstName) and (PersonDao.lastName eq entity.lastName)
            }
            .mapNotNull { it.toEntity() }
            .firstOrNull()
            .run {
                return@transaction when {
                    this == null -> {
                        PersonDao.insertOne(entity)
                    }
                    else -> {
                        return@transaction this
                    }
                }
            }
    }

    fun addMultiple(persons: List<PersonAccess>) = transaction {
        PersonDao.batchInsert(persons) { person ->
            val now = LocalDateTime.now()
            this.from(person, PersonDao)
            this[PersonDao.createdAt] = now
            this[PersonDao.modifiedAt] = now
        }.map { it.toEntity() }
    }

    override fun delete(entity: PersonAccess) = transaction {
        PersonDao.deleteIgnoreWhere { PersonDao.id eq entity.id }
        commit()
    }

    override fun update(entity: PersonAccess): PersonAccess = transaction {
        PersonDao.updateOne(entity) {
            PersonDao.id eq entity.id
        }
    }

    override fun findOneById(id: Int): PersonAccess? = transaction {
        PersonDao.select { PersonDao.id eq id }.mapNotNull { it.toEntity() }.firstOrNull()
    }

    fun findByName(lastName: String, firstName: String?, middleName: String?, count: Int, offset: Int) = transaction {
        var query = PersonDao.select { PersonDao.lastName like lastName }
        query = firstName?.let {
            query.andWhere { PersonDao.firstName like it }
        } ?: query

        query = middleName?.let {
            query.andWhere { PersonDao.middleName like middleName }
        } ?: query

        query.limit(count, offset.toLong()).mapNotNull { it.toEntity() }
    }

    override fun findAllAndPaginate(count: Int, offset: Int): List<PersonAccess> = transaction {
        PersonDao.findAllAndPaginate(count, offset)
    }

    override fun UpdateBuilder<Any>.from(entity: PersonAccess, dao: PersonDao) {
        this[dao.id] = entity.id
        this[dao.firstName] = entity.firstName
        this[dao.middleName] = entity.middleName
        this[dao.lastName] = entity.lastName
        this[dao.gender] = entity.gender
        this[dao.mobileNumber] = entity.mobileNo
        this[dao.address] = entity.address
        this[dao.birthday] = entity.birthday
        this[dao.createdBy] = entity.createdBy
        this[dao.modifiedBy] = entity.modifiedBy
    }

    override fun ResultRow.toEntity(): PersonAccess {
        val personAccess = PersonAccess()
        personAccess.id = this[PersonDao.id]
        personAccess.firstName = this[PersonDao.firstName]
        personAccess.middleName = this[PersonDao.middleName]
        personAccess.lastName = this[PersonDao.lastName]
        personAccess.gender = this[PersonDao.gender]
        personAccess.mobileNo = this[PersonDao.mobileNumber]
        personAccess.birthday = this[PersonDao.birthday]
        personAccess.address = this[PersonDao.address]
        // removed files from created at and modified at

        return personAccess
    }

    fun createAccessFromAliasTable(resultRow: ResultRow, table: Alias<PersonDao>): PersonAccess? {
        val id = resultRow.getOrNull(table[PersonDao.id])

        return if (id != null) {
            val personAccess = PersonAccess()
            personAccess.id = id
            personAccess.firstName = resultRow[table[PersonDao.firstName]]
            personAccess.middleName = resultRow[table[PersonDao.middleName]]
            personAccess.lastName = resultRow[table[PersonDao.lastName]]
            personAccess.gender = resultRow[table[PersonDao.gender]]
            personAccess.address = resultRow[table[PersonDao.address]]
            personAccess.mobileNo = resultRow[table[PersonDao.mobileNumber]]
            personAccess.birthday = resultRow[table[PersonDao.birthday]]
            personAccess.createdAt = resultRow[table[PersonDao.createdAt]]
            personAccess.modifiedAt = resultRow[table[PersonDao.modifiedAt]]
            return personAccess
        } else null
    }

    infix fun fromResultRow(resultRow: ResultRow): PersonAccess {
        return resultRow.toEntity()
    }
}
