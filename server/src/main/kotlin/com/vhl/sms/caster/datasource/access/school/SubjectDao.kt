package com.vhl.sms.caster.datasource.access.school

import com.vhl.sms.caster.commons.model.school.Subject
import com.vhl.sms.caster.datasource.access.UserDoa
import com.vhl.sms.caster.exposed.Dao
import com.vhl.sms.caster.exposed.Domain
import java.time.LocalDateTime

object SubjectDao: Dao("subject") {
    val gradeLevel = integer("grade_level")
    val subjectName = varchar("subject_name", 50)
    val student = integer("student_id") references StudentDao.id
    val adviser = integer("adviser_id") references AdviserDao.id
    val advisory = integer("advisory_id") references AdvisoryDao.id
    val description = varchar("description", 150).nullable()
    val grading1 = integer("grading_1").nullable()
    val grading2 = integer("grading_2").nullable()
    val grading3 = integer("grading_3").nullable()
    val grading4 = integer("grading_4").nullable()

    val createdBy = integer("created_by").references(UserDoa.id).nullable()
    val modifiedBy = integer("modified_by").references(UserDoa.id).nullable()
}

class SubjectAccess: Domain {

    override var id: Int = 0
    var gradeLevel: Int = 0
    var subjectName: String = ""
    var student: StudentAccess? = null
    var adviser: AdviserAccess? = null
    var advisory: AdvisoryAccess? = null
    var description: String? = ""
    var grading1: Int? = null
    var grading2: Int? = null
    var grading3: Int? = null
    var grading4: Int? = null

    var createdBy: Int? = null
    var modifiedBy: Int? = null

    override var createdAt: LocalDateTime? = null
    override var modifiedAt: LocalDateTime? = null

    infix fun from(subject: Subject): SubjectAccess {
        this.id = subject.id
        this.subjectName = subject.subjectName
        this.student = StudentAccess() from subject.student
        this.adviser = AdviserAccess() from subject.adviser
        this.description = subject.description
        this.grading1 = subject.grading1
        this.grading2 = subject.grading2
        this.grading3 = subject.grading3
        this.grading4 = subject.grading4

        return this
    }

    infix fun itsId(id: Int): SubjectAccess {
        this.id = id
        return this
    }

    fun toSubject(): Subject {
        return Subject(
            id,
            gradeLevel,
            subjectName,
            student?.toStudent() ?: throw IllegalArgumentException("student not initialize"),
            adviser?.toAdviser() ?: throw IllegalArgumentException("adviser not initialize"),
            advisory?.toAdvisory()?: throw IllegalArgumentException("adviser not initialize"),
            description,
            grading1,
            grading2,
            grading3,
            grading4

        )
    }
}