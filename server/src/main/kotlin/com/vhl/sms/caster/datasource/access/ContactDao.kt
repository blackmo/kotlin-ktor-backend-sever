package com.vhl.sms.caster.datasource.access

import com.vhl.sms.caster.commons.model.Contact
import com.vhl.sms.caster.exposed.Dao
import com.vhl.sms.caster.exposed.Domain
import java.time.LocalDateTime

object ContactDao : Dao("contact") {
    val name        = varchar("name", 150)
    val number      = varchar("number", 15)
    val userId       = (integer("user_id")) references (UserDoa.id)
}

class ContactAccess: Domain {
    override var id: Int = 0
    var name: String = ""
    var number: String = ""
    var userId: Int = 0

    override var createdAt: LocalDateTime? = null
    override var modifiedAt: LocalDateTime? = null

    infix fun from(contact: Contact): ContactAccess {
        contact.id?.let {
            id = it
        }
        name = contact.name
        number = contact.number
        userId = contact.userId
        return this
    }

    infix fun itsID(id: Int): ContactAccess {
        this.id = id
        return this
    }
}
