package com.vhl.sms.caster.service

import com.vhl.sms.caster.commons.payload.common.ChangePassword
import com.vhl.sms.caster.commons.response.Response
import com.vhl.sms.caster.commons.service.EncryptionService
import com.vhl.sms.caster.datasource.access.UserAccess
import com.vhl.sms.caster.repository.UserRepository
import com.vhl.sms.caster.utils.StatusResponse
import com.vhl.sms.caster.utils.handler.error.responseCatch

@Suppress("OVERRIDE_BY_INLINE")
class UserService (
    val userRepository: UserRepository,
    encryption: EncryptionService
): ServiceBase<UserAccess> {

    val useEncryption = encryption.getEncryptionType(EncryptionService.EncryptionType.DEFAULT)

    override inline fun add(
        entity: UserAccess,
        createdBy: Int,
        response: (code: String, message: String, dataValue: UserAccess?) -> Response<UserAccess>
    ): Response<UserAccess> {
        return responseCatch {
            entity.password = useEncryption.encrypt(entity.password)
            val dataValue = userRepository.add(entity)
            response(StatusResponse.SUCCESS, "Successfully registered user", dataValue)
        }
    }

    override inline fun delete(
        entity: UserAccess,
        response: (code: String, message: String, dataValue: UserAccess?) -> Response<UserAccess>
    ): Response<UserAccess> {
        return responseCatch {
            userRepository.delete(entity)
            response(StatusResponse.SUCCESS, "Successfully deleted user", null)
        }
    }

    override inline fun update(
        entity: UserAccess,
        modifiedBy: Int,
        response: (code: String, message: String, dataValue: UserAccess?) -> Response<UserAccess>
    ): Response<UserAccess> {
        return responseCatch {
            entity.password = useEncryption.encrypt(entity.password)
            val dataValue = userRepository.update(entity)
            response(StatusResponse.SUCCESS, "Successfully updated user account", dataValue)
        }
    }

    override inline fun findOneById(
        id: Int,
        response: (code: String, message: String, dataValue: UserAccess?) -> Response<UserAccess>
    ): Response<UserAccess> {
        return responseCatch {
            val dataValue = userRepository.findOneById(id)
            response(StatusResponse.SUCCESS, "", dataValue)
        }
    }

    override inline fun findAllAngPaginate(
        count: Int,
        offset: Int,
        response: (code: String, message: String, dataValue: List<UserAccess>?) -> Response<List<UserAccess>>
    ): Response<List<UserAccess>> {
        return responseCatch {
            val dataValue = userRepository.findAllAndPaginate(count, offset)
            response(StatusResponse.SUCCESS, "", dataValue)
        }
    }

    fun updatePassword(
        changePassword: ChangePassword,
        userId: Int,
        response: (code: String, message: String, dataValue: String?) -> Response<String>
    ): Response<String> {
        return responseCatch {
            val equal = userRepository.findOneById(userId)?.let {
                it.password = useEncryption.decrypt(it.password)
                it.password ==  changePassword.oldPassword
            }!!

            return when {
                !equal -> {
                    response(StatusResponse.FAILED, "Invalid Input Password", "Failed")
                }
                else -> {
                    userRepository.updatePassword(useEncryption.encrypt(changePassword.newPassword), userId)
                    response(StatusResponse.SUCCESS, "Successfully Updated Password", "SUCCESS")
                }
            }
        }
    }

    inline fun validateLogin(
        userAccess: UserAccess,
        response: (code: String, message: String, dataValue: UserAccess?) -> Response<UserAccess>
    ): Response<UserAccess> {
        return responseCatch {
            val retrieved = userRepository.findOneByUserName(userAccess.userName)
            try {
                if (retrieved != null) {
                    if (userAccess.password == useEncryption.decrypt(retrieved.password)) {
                        return response(StatusResponse.SUCCESS, "verified", retrieved)
                    }
                }
            } catch (e: IllegalArgumentException) {
                return response(StatusResponse.FAILED, "Invalid User Login", null)
            }
            response(StatusResponse.FAILED, "User Not found", null)
        }
    }

}