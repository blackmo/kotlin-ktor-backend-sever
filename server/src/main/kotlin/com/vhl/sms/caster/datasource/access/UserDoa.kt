package com.vhl.sms.caster.datasource.access

import com.vhl.sms.caster.commons.model.User
import com.vhl.sms.caster.commons.model.enums.Privileges
import com.vhl.sms.caster.datasource.access.school.PersonAccess
import com.vhl.sms.caster.datasource.access.school.PersonDao
import com.vhl.sms.caster.exposed.Dao
import com.vhl.sms.caster.exposed.Domain
import java.time.LocalDateTime


object UserDoa: Dao("user") {
    val userName = varchar("user_name", 50).uniqueIndex()
    val password = varchar("user_password", 50)
    val person = (integer("person_id") references PersonDao.id).nullable()
    val privilege = enumerationByName("user_privilege", 20, Privileges::class).default(Privileges.ADVISER)
}

class UserAccess: Domain {
    override var id: Int = 0
    var userName: String = ""
    var password: String = ""
    var person: PersonAccess? = null
    var privilege = Privileges.ADVISER

    override var createdAt: LocalDateTime? = null
    override var modifiedAt: LocalDateTime? = null

    infix fun from(user: User): UserAccess {
        userName = user.userName
        password = user.password
        person = user.person?.let { PersonAccess() from it }
        privilege = user.privileges

        return this
    }

    fun toUser(): User = User(
        id,
        userName,
        password,
        person?.toPerson(),
        privilege
    )
}

