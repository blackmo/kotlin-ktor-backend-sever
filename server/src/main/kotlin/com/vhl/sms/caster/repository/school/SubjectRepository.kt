package com.vhl.sms.caster.repository.school

import com.vhl.sms.caster.commons.errors.AlreadyExistError
import com.vhl.sms.caster.datasource.access.school.*
import com.vhl.sms.caster.exposed.ExposedSchema
import com.vhl.sms.caster.repository.DataRepository
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.UpdateBuilder
import org.jetbrains.exposed.sql.transactions.transaction

class SubjectRepository(
    private val studentRepository: StudentRepository,
    private val adviserRepository: AdviserRepository
):
    ExposedSchema<SubjectAccess, SubjectDao>(), DataRepository<SubjectAccess> {

    override val entityName: String = "Subject"
    override val originName: String = "ORIGIN_SUBJECT"

    companion object {
        private const val ALIAS_STUDENT = "person"
        private const val ALIAS_ADVISER = "mother"
    }

    private val personStudent = PersonDao.alias(ALIAS_STUDENT)
    private val personAdviser = PersonDao.alias(ALIAS_ADVISER)

    private val fieldSets = mutableListOf<Expression<*>>()
        .also {
            it.add(SubjectDao.id)
            it.add(SubjectDao.gradeLevel)
            it.add(SubjectDao.subjectName)
            it.add(SubjectDao.description)
            it.add(SubjectDao.grading1)
            it.add(SubjectDao.grading2)
            it.add(SubjectDao.grading3)
            it.add(SubjectDao.grading4)
            it.addAll(advisorySoftFieldSet())
            it.addAll(personFieldWithAlias(personStudent))
            it.addAll(personFieldWithAlias(personAdviser))
        }

    override fun add(entity: SubjectAccess): SubjectAccess = transaction {
        SubjectDao.checkIfAlreadyExist {
            (SubjectDao.id eq entity.id)
                .or(
                    (SubjectDao.subjectName eq entity.subjectName)
                        .and(SubjectDao.student eq entity.student!!.id)
                        .and(SubjectDao.gradeLevel eq entity.gradeLevel)
                )
        }.run {
            if (this) throw AlreadyExistError("Subject Id already exist", originName)
        } 
        SubjectDao.insertOne(entity)
    }

    override fun delete(entity: SubjectAccess) = transaction{
        SubjectDao.deleteWhere { SubjectDao.id eq entity.id }
        commit()
    }

    override fun update(entity: SubjectAccess): SubjectAccess = transaction {
       SubjectDao.updateOne(entity) {
           SubjectDao.id eq entity.id
       }
    }

    override fun findOneById(id: Int): SubjectAccess?  = transaction {
        SubjectDao.select { SubjectDao.id eq id }.mapNotNull { it.toEntity() }.firstOrNull()
    }

    fun findOneEager(lrnId: String) {
        findByEagerAction { StudentDao.lrnID eq lrnId }
            .mapNotNull { it.joinWithStudentAdviser() }
            .firstOrNull()
    }

    fun findOneByEager(advisoryName: String, advisoryId: Int?) = transaction {
        val query = findByEagerAction { (AdvisoryDao.advisoryName eq advisoryName ) }
        val resultQuery = advisoryId?.let { query.orWhere { AdvisoryDao.id eq it } } ?: query

        resultQuery
            .orderBy(personStudent[PersonDao.lastName], SortOrder.ASC)
            .map { it.joinWithStudentAdviser() }
    }

    private fun findByEagerAction(where: SqlExpressionBuilder.()-> Op<Boolean> ) =
        SubjectDao
            .innerJoin(StudentDao, {student}, { id })
            .innerJoin(AdviserDao, {SubjectDao.adviser}, {id})
            .innerJoin(personStudent, {StudentDao.id}, {personStudent[PersonDao.id]})
            .innerJoin(personStudent, {AdviserDao.id}, {personAdviser[PersonDao.id]})
            .innerJoin(AdvisoryDao, {SubjectDao.advisory}, {id})
            .slice(fieldSets)
            .select(where)

    override fun findAllAndPaginate(count: Int, offset: Int): List<SubjectAccess> = transaction {
        SubjectDao.findAllAndPaginate(count, offset)
    }

    override fun UpdateBuilder<Any>.from(entity: SubjectAccess, dao: SubjectDao) {
        this[dao.description] = entity.description
        this[dao.gradeLevel] = entity.gradeLevel
        this[dao.subjectName] = entity.subjectName
        this[dao.grading1] = entity.grading1
        this[dao.grading2] = entity.grading2
        this[dao.grading3] = entity.grading3
        this[dao.grading4] = entity.grading4
        this[dao.createdBy] = entity.createdBy
        this[dao.modifiedBy] = entity.modifiedBy

        entity.adviser?.id?.let { this[dao.adviser] = it }
        entity.student?.id?.let { this[dao.student] = it }
    }

    override fun ResultRow.toEntity(): SubjectAccess {
        val subjectAccess = SubjectAccess()

        subjectAccess.id = this[SubjectDao.id]
        subjectAccess.gradeLevel = this[SubjectDao.gradeLevel]
        subjectAccess.subjectName = this[SubjectDao.subjectName]
        subjectAccess.student = StudentAccess() itsID this[SubjectDao.student]
        subjectAccess.adviser = AdviserAccess() itsID this[SubjectDao.adviser]
        subjectAccess.advisory = AdvisoryAccess() itsID  this[SubjectDao.advisory]
        subjectAccess.description = this[SubjectDao.description]
        subjectAccess.grading1 = this[SubjectDao.grading1]
        subjectAccess.grading2 = this[SubjectDao.grading2]
        subjectAccess.grading3 = this[SubjectDao.grading3]
        subjectAccess.grading4 = this[SubjectDao.grading4]

        return subjectAccess
    }

    private fun ResultRow.joinWithStudentAdviser(): SubjectAccess {

        val subjectAccess = SubjectAccess()

        subjectAccess.id = this[SubjectDao.id]
        subjectAccess.gradeLevel = this[SubjectDao.gradeLevel]
        subjectAccess.subjectName = this[SubjectDao.subjectName]
        subjectAccess.student = studentRepository.fromResultRow(this)
        subjectAccess.adviser = adviserRepository.fromResultRow(this)
        subjectAccess.description = this[SubjectDao.description]
        subjectAccess.grading1 = this[SubjectDao.grading1] ?: 0
        subjectAccess.grading2 = this[SubjectDao.grading2] ?: 0
        subjectAccess.grading3 = this[SubjectDao.grading3] ?: 0
        subjectAccess.grading4 = this[SubjectDao.grading4] ?: 0

        return subjectAccess
    }
}
