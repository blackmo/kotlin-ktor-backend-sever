package com.vhl.sms.caster.di

import com.vhl.sms.caster.di.module.*
import getProperty
import io.ktor.application.ApplicationEnvironment
import io.ktor.util.KtorExperimentalAPI
import org.kodein.di.Kodein

@KtorExperimentalAPI
class LoadedModules(private val env: ApplicationEnvironment) {
    val kodein = Kodein {
        val serverPassword = env.getProperty("encryption.server.password")
        val serverSalt = env.getProperty("encryption.server.salt")

        val rdPort = env.getProperty("redis.port").toInt()
        val rdHost = env.getProperty("redis.host")
        val rdPassword = env.getProperty("redis.password")

        val secret = env.getProperty("jwt.secret")
        val expiration = env.getProperty("jwt.expiration").toLong() * env.getProperty("jwt.multH").toInt()

        val destination = env.getProperty("upload.csv.destination")
        val csvNamingConvention = env.getProperty("upload.csv.namingConvention")

        //-- authentication
        import(AuthModule.authentication(secret, expiration))

        // -- redis
        import(SessionModules.redisDB1Module(rdHost, rdPort, rdPassword))
        import(SessionModules.redisDB2Module(rdHost, rdPort, rdPassword))
        import(SessionModules.tokenSessionService())
        // -- encryption
        import(ServerModule.serverEncryption(serverPassword, serverSalt))
        // -- web sockets
        import(ServerModule.webSocketModule())
        import(ServerModule.updateDeviceSocketService())

        //-- user
        import(ServerModule.userModule())

        //-- school modules
        import(SchoolModule.personRouteModule())
        import(SchoolModule.studentRouteModule())
        import(SchoolModule.adviserRouteModule())
        import(SchoolModule.advisoryRouteModule())
        import(SchoolModule.refAdvisoryModule())

        //-- upload school module
        import(UploadModule.uploadCSVModule(destination, csvNamingConvention))
    }
}

