package com.vhl.sms.caster.di.module

import com.vhl.sms.caster.Logger
import com.vhl.sms.caster.constants.ModuleTags
import com.vhl.sms.caster.service.TokenSessionService
import com.vhl.sms.caster.storage.redis.Redis
import io.lettuce.core.RedisConnectionException
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import kotlin.system.exitProcess

class SessionModules {
    companion object {
        fun redisDB1Module(host: String, port: Int, password: String? = null) = Kodein.Module("redis-db1") {
            bind(tag = ModuleTags.REDIS_DB1) from singleton {
                try {
                    Redis.createClient(host, port, 1, password)
                } catch (e: RedisConnectionException) {
                    e.printStackTrace()
                    Logger.error { "redis server is unreachable. Application Terminated" }
                    exitProcess(1)
                }
            }
        }

        fun redisDB2Module(host: String, port: Int, password: String? = null) = Kodein.Module("redis-db2") {
            bind(tag = ModuleTags.REDIS_DB2) from singleton {
                try {
                    Redis.createClient(host, port, 2, password)
                } catch (e: RedisConnectionException) {
                    e.printStackTrace()
                    Logger.error { "redis server is unreachable. Application Terminated" }
                    exitProcess(1)
                }
            }
        }

        fun tokenSessionService() = Kodein.Module("token-session-service") {
            bind() from singleton {
                TokenSessionService(instance(ModuleTags.REDIS_DB2))
            }
        }
    }
}