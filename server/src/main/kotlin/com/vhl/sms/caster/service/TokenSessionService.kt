package com.vhl.sms.caster.service

import com.vhl.sms.caster.storage.redis.Redis
import extractTokenModel

class TokenSessionService(
    private val redis2: Redis
) {

    companion object {
        const val KEY_LOGGED_IN = "logged_in::"
        const val KEY_BLACK_LIST = "black_listed_token::"
    }

    fun blackListToken(token: String, expiresAt: Long) {
        redis2.async.set("$KEY_BLACK_LIST$token", expiresAt.toString())
        redis2.async.expireat("$KEY_BLACK_LIST$token", expiresAt)
    }

    fun setLoggedInUser(userId: Int, token: String, expires: Long) {
        redis2.async.set("$KEY_LOGGED_IN$userId", token)
        redis2.async.expire("$KEY_LOGGED_IN$userId", expires / 1000)
    }

    fun onLoggedUser(token: String): Boolean {
        return extractTokenModel(token)?.let {
            !redis2.async.get("$KEY_LOGGED_IN${it.id}").get().isNullOrBlank()
        } ?: false
    }

    fun onLoggedUser(userId: Int): String? = redis2.async.get("$KEY_LOGGED_IN$userId").get()

    fun removeActiveUser(token: String) {
        extractTokenModel(token)?.run {
            redis2.async.del("$KEY_LOGGED_IN${id}")
        }
    }

    fun onBlackListToken(token: String): Boolean = !redis2.async.get("$KEY_BLACK_LIST$token").get().isNullOrEmpty()

    fun flushDB() {
        redis2.async.flushdb()
    }
}