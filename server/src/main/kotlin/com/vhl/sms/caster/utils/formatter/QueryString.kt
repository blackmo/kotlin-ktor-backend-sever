package com.vhl.sms.caster.utils.formatter

fun String.notEmptyLike() = when {
    this.isEmpty() -> ""
    else -> "$this%"
}

