package com.vhl.sms.caster.service.school

import com.vhl.sms.caster.commons.model.school.Advisory
import com.vhl.sms.caster.commons.payload.outgoing.AdvisoryJoinAdviserPrsn
import com.vhl.sms.caster.commons.payload.school.adviory.SearchAdvisoryP
import com.vhl.sms.caster.commons.response.Response
import com.vhl.sms.caster.datasource.access.school.AdvisoryAccess
import com.vhl.sms.caster.repository.school.AdvisoryRepository
import com.vhl.sms.caster.repository.school.advisory.RefAdvisoryStudentRepository
import com.vhl.sms.caster.service.ServiceBase
import com.vhl.sms.caster.utils.StatusResponse
import com.vhl.sms.caster.utils.handler.error.responseCatch

class AdvisoryService(
    private val advisoryRepository: AdvisoryRepository,
    private val refAdvisoryStudentRepository: RefAdvisoryStudentRepository
) : ServiceBase<Advisory> {
    override fun add(
        entity: Advisory,
        createdBy: Int,
        response: (code: String, message: String, dataValue: Advisory?) -> Response<Advisory>
    ): Response<Advisory> {
        return responseCatch {
            val advisoryAccess = AdvisoryAccess().from(entity).apply { this.createdBy = createdBy }
            val data = advisoryRepository.add(advisoryAccess)
            response(StatusResponse.SUCCESS, "Successfully Saved Advisory", data.toAdvisory())
        }
    }

    override fun delete(
        entity: Advisory,
        response: (code: String, message: String, dataValue: Advisory?) -> Response<Advisory>
    ): Response<Advisory> {
        return responseCatch {
            advisoryRepository.add(AdvisoryAccess() from entity)
            response(StatusResponse.SUCCESS, "Successfully Deleted Advisory", null)
        }
    }

    override fun update(
        entity: Advisory,
        modifiedBy: Int,
        response: (code: String, message: String, dataValue: Advisory?) -> Response<Advisory>
    ): Response<Advisory> {
        return responseCatch {
            val advisoryAccess = AdvisoryAccess().from(entity).apply { this.modifiedBy = modifiedBy }
            val data = advisoryRepository.update(advisoryAccess)
            response(StatusResponse.SUCCESS, "Successfully Updated Advisory", data.toAdvisory())
        }
    }

    override fun findOneById(
        id: Int,
        response: (code: String, message: String, dataValue: Advisory?) -> Response<Advisory>
    ): Response<Advisory> {
        return responseCatch {
            val data = advisoryRepository.findOneByIdEager(id)
            data?.listStudentAccess = refAdvisoryStudentRepository.findStudentOnAdvisory(id)
            response(StatusResponse.SUCCESS, "Successfully Retrieved Advisory", data?.toAdvisory())
        }
    }

    override fun findAllAngPaginate(
        count: Int,
        offset: Int,
        response: (code: String, message: String, dataValue: List<Advisory>?) -> Response<List<Advisory>>
    ): Response<List<Advisory>> {
        return responseCatch {
            val data = advisoryRepository.findAllAndPaginate(count, offset)
            response(StatusResponse.SUCCESS, "Successfully Retrieved Advisory", data.map { it.toAdvisory() })
        }
    }

    fun findByCriteria(
        criteria: SearchAdvisoryP,
        response: (code: String, message: String, dataValue: List<AdvisoryJoinAdviserPrsn>?) -> Response<List<AdvisoryJoinAdviserPrsn>>
    ): Response<List<AdvisoryJoinAdviserPrsn>> {
        return responseCatch {
            val data = advisoryRepository.findBySearchSCriteria(criteria)
            response(StatusResponse.SUCCESS, "Successfully Retrieved Advisory Records", data)
        }
    }
}