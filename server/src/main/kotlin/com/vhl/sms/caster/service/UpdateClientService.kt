package com.vhl.sms.caster.service

import com.beust.klaxon.Klaxon
import com.vhl.sms.caster.Logger
import com.vhl.sms.caster.commons.commands.WSConstants
import com.vhl.sms.caster.commons.errors.DataNotFoundError
import com.vhl.sms.caster.commons.model.MessageToSend
import com.vhl.sms.caster.commons.response.Response
import com.vhl.sms.caster.handler.SocketSessionHandler
import com.vhl.sms.caster.utils.StatusResponse

class UpdateClientService(
    val mobileSocketSessionHandler: SocketSessionHandler
) {
    inline fun sendMessageToDevice(
        deviceId: String,
        messageToSend: MessageToSend,
        response: (code: String, message: String, dataValue: Any?) -> Response<Any>
    ): Response<Any> {
        return try {
            val device = mobileSocketSessionHandler.getSocketLifeStatus(deviceId)
                ?: throw DataNotFoundError("Device Not Found", "U_MOBILE_CLIENT")
            val jsonMessage = Klaxon().toJsonString(messageToSend)
            device.commands.add(WSConstants.DVC_RETRIEVE_MESSAGE_TO_SEND + jsonMessage)
            response(StatusResponse.SUCCESS, "Message Successfully Queued", null)
        } catch (e: Exception) {
            Logger.error { e.message }
            response(StatusResponse.FAILED, e.message!!, null)
        }
    }
}