package com.vhl.sms.caster.controller.web.school

import com.vhl.sms.caster.commons.model.school.Student
import com.vhl.sms.caster.commons.payload.school.student.SearchStudentP
import com.vhl.sms.caster.commons.response.Response
import com.vhl.sms.caster.features.validateToken
import com.vhl.sms.caster.service.school.StudentService
import com.vhl.sms.caster.utils.StatusResponse
import getTokenModel
import io.ktor.application.call
import io.ktor.auth.authenticate
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.Routing
import io.ktor.routing.post

class StudentController(
    private val studentService: StudentService
) {
    fun createRoute(root: String, routing: Routing) {
        val base = "$root/school/student"
        routing {
            validateToken {
                authenticate {
                    registerStudent("$base/register")
                    findStudent("$base/search")
                    findByLrnIdOrLastName("$base/search/by")
                    updateStudent("$base/update")
                }
            }
        }
    }

    private fun Route.registerStudent(url: String) {
        post(url) {
            val dataResponse: Response<Student>
            val student = call.receive<Student>()
            val tokenModel = call.request.getTokenModel()!!

            dataResponse = studentService.add(student, tokenModel.id) { code, message, dataValue ->
                Response(code, message, dataValue)
            }

            if (dataResponse.code != StatusResponse.SUCCESS) {
                call.response.status(HttpStatusCode.BadRequest)
            }
            call.respond(dataResponse)
        }
    }

    private fun Route.findStudent(url: String) {
        post(url) {
            val dataResponse: Response<List<Student>>
            val searchBody = call.receive<SearchStudentP>()

            if (searchBody.byLrnId) {
                when {
                    searchBody.lrnID.isNullOrEmpty() -> {
                        call.response.status(HttpStatusCode.BadRequest)
                        dataResponse = Response(StatusResponse.ERROR, "No LRND Given", null)
                        call.respond(dataResponse)
                        return@post
                    }
                    else -> {
                        dataResponse =
                            studentService.findByEager(searchBody.lrnID!!, null) { code, message, dataValue ->
                                Response(code, message, dataValue)
                            }

                        call.respond(dataResponse)
                        return@post
                    }
                }
            }
        }
    }

    private fun Route.findByLrnIdOrLastName(url: String) {
        post(url) {
            val dataResponse: Response<List<Student>>
            val criteria = call.receive<SearchStudentP>()

            dataResponse = studentService.findByLrnIdOrLastName(
                criteria.lrnID!!,
                criteria.lastName!!,
                criteria.status
            ) { code, message, dataValue ->
                Response(code, message, dataValue)
            }

            if (dataResponse.code != StatusResponse.SUCCESS) {
                call.response.status(HttpStatusCode.BadRequest)
            }

            call.respond(dataResponse)
        }
    }

    private fun Route.updateStudent(url: String) {
        post(url) {
            val dataResponse: Response<Student>
            val student = call.receive<Student>()
            val tokenModel = call.request.getTokenModel()!!

            dataResponse = studentService.update(student, tokenModel.id) { code, message, dataValue ->
                Response(code, message, dataValue)
            }

            if (dataResponse.code != StatusResponse.SUCCESS) {
                call.response.status(HttpStatusCode.BadRequest)
            }

            call.respond(dataResponse)
        }
    }
}
