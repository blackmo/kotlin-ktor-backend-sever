package com.vhl.sms.caster.service.school

import com.vhl.sms.caster.commons.errors.E_ALREADY_EXIST
import com.vhl.sms.caster.commons.errors.FailedInsertError
import com.vhl.sms.caster.commons.model.school.Student
import com.vhl.sms.caster.commons.response.Response
import com.vhl.sms.caster.commons.warnings.W_ALREADY_EXIST
import com.vhl.sms.caster.datasource.access.school.PersonAccess
import com.vhl.sms.caster.datasource.access.school.StudentAccess
import com.vhl.sms.caster.repository.school.StudentRepository
import com.vhl.sms.caster.service.ServiceBase
import com.vhl.sms.caster.utils.StatusResponse
import com.vhl.sms.caster.utils.handler.error.responseCatch

class StudentService(
    private val studentRepository: StudentRepository,
    private val personService: PersonService
) : ServiceBase<Student> {
    override fun add(
        entity: Student,
        createdBy: Int,
        response: (code: String, message: String, dataValue: Student?) -> Response<Student>
    ): Response<Student> {
        return studentRepository.attemptSaveStudentDetails(entity, createdBy)
    }

    override fun delete(
        entity: Student,
        response: (code: String, message: String, dataValue: Student?) -> Response<Student>
    ): Response<Student> {
        return responseCatch { 
            studentRepository.delete(entity.toAccess())
            response(StatusResponse.SUCCESS, "Successfully Deleted Student", null)
         }
    }

    override fun update(
        entity: Student,
        modifiedBy: Int,
        response: (code: String, message: String, dataValue: Student?) -> Response<Student>
    ): Response<Student> {
        return responseCatch {
            val dataAccess = StudentAccess().from(entity).also { it.modifiedBy = modifiedBy }
            val data = studentRepository.update(dataAccess)
            response(StatusResponse.SUCCESS, "Successfully Updated Student", data.toStudent())
        }
    }

    override fun findOneById(
        id: Int,
        response: (code: String, message: String, dataValue: Student?) -> Response<Student>
    ): Response<Student> {
        return responseCatch { 
            val data = studentRepository.findOneById(id)
            response(StatusResponse.SUCCESS, "Successfully Retrieved Student", data?.toStudent())
        }
    }

    override fun findAllAngPaginate(
        count: Int,
        offset: Int,
        response: (code: String, message: String, dataValue: List<Student>?) -> Response<List<Student>>
    ): Response<List<Student>> {
        return responseCatch { 
            val data = studentRepository.findAllAndPaginate(count, offset)
            response(StatusResponse.SUCCESS, "Successfully Retrieved Students", data.map { it.toStudent() })
        }
    }

    fun findByEager(
        lrnID: String,
        studentId: Int?,
        response: (code: String, message: String, dataValue: List<Student>?) -> Response<List<Student>>
    ): Response<List<Student>> {
        return responseCatch {
            val data = studentRepository.findByEager(lrnID, studentId)
            response(StatusResponse.SUCCESS, "Successfully Retrieved Students", data.map { it.toStudent() })
        }
    }

    fun findByLrnIdOrLastName(
        lrnID: String,
        lastName: String,
        status: String?,
        response: (code: String, message: String, dataValue: List<Student>) -> Response<List<Student>>
    ): Response<List<Student>> {
        return responseCatch {
            val data = studentRepository.findByLrnIdOrLastName(lrnID, lastName, status)
            response(StatusResponse.SUCCESS, "Successfully Retrieved Students", data.map { it.toStudent() })
        }
    }

    private fun Student.toAccess() = StudentAccess() from this

    // -- private methods
    private fun StudentRepository.attemptSaveStudentDetails(student: Student, createdBy: Int = 0): Response<Student> =
        responseCatch {
            val personAccess = PersonAccess().from(student.person).also { it.createdBy = createdBy }
            val person = personService.addOrGet(personAccess) { code, message, dataValue ->
                when (code) {
                    StatusResponse.SUCCESS -> Response(code, message, dataValue)
                    E_ALREADY_EXIST -> Response(W_ALREADY_EXIST, "Student Data Already Exist", student.person)
                    else -> throw FailedInsertError(message, "ST_SVC")
                }
            }

            val father = student.father?.let {
                val fatherAccess = PersonAccess().from(it).also { it.createdBy = createdBy }
                personService.addOrGet(fatherAccess) { code, message, dataValue ->
                    when (code) {
                        StatusResponse.SUCCESS -> Response(code, message, dataValue)
                        E_ALREADY_EXIST -> Response(W_ALREADY_EXIST, "Father Details Already Exist", dataValue)
                        else -> throw FailedInsertError(message, "ST_SVC")
                    }
                }
            }

        val mother = student.mother?.let {
            val motherAccess = PersonAccess().from(it).also { it.createdBy = createdBy }
            personService.addOrGet(motherAccess) { code, message, dataValue ->
                when (code) {
                    StatusResponse.SUCCESS -> Response(code, message, dataValue)
                    E_ALREADY_EXIST -> Response(W_ALREADY_EXIST, "Mother Details Already Exist", dataValue)
                    else -> throw FailedInsertError(message, "ST_SVC")
                }
            }
        }

            val studentInput = Student(student.id, student.lrnID, person.data!!, mother?.data, father?.data)

            val messageMother = mother?.message ?: "N_MOTHER"
            val messageFather = father?.message ?: "N_FATHER"

            val studentAccess = StudentAccess().from(studentInput).also { it.createdBy = createdBy }
            val registeredStudent = add(studentAccess).toStudent()

            return Response(
                StatusResponse.SUCCESS,
                "Student Created || ${person.message} || $messageMother || $messageFather ",
                registeredStudent
            )
    }
}