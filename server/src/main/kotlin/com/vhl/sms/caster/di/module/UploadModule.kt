package com.vhl.sms.caster.di.module

import com.vhl.sms.caster.controller.web.school.UploadController
import com.vhl.sms.caster.service.upload.UploadService
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider

class UploadModule {
    companion object {
        fun uploadCSVModule(downloadDestination: String, namingConvention: String) = Kodein.Module("upload-module") {
            bind() from provider { UploadService(instance()) }
            bind() from provider { UploadController(instance(), downloadDestination, namingConvention) }
        }
    }
}