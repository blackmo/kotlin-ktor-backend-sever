package com.vhl.sms.caster.datasource.access.school

import com.vhl.sms.caster.commons.model.school.Advisory
import com.vhl.sms.caster.commons.model.school.Student
import com.vhl.sms.caster.datasource.access.UserDoa
import com.vhl.sms.caster.exposed.Dao
import com.vhl.sms.caster.exposed.Domain
import org.jetbrains.exposed.sql.Expression
import java.time.LocalDateTime

object AdvisoryDao: Dao("advisory") {
    val classYear = integer("class_year")
    val classLevel = integer("class_level")
    val advisoryName = varchar("advisory_name", 50)
    val sectionName = varchar("section_name", 50).nullable()
    val sectionNumber = integer("section_number")
    val adviser = integer("adviser_id") references AdviserDao.id
    val listStudentId = varchar("list_student", 1000)

    val createdBy = integer("created_by").references(UserDoa.id).nullable()
    val modifiedBy = integer("modified_by").references(UserDoa.id).nullable()
}

class AdvisoryAccess: Domain {

    override var id: Int = 0
    var classYear: Int = 0
    var classLevel: Int = 0
    var advisoryName: String = ""
    var sectionName: String? = ""
    var sectionNumber: Int = 0
    var adviser: AdviserAccess? = null
    var listStudentId: String = ""
    var listStudentAccess: List<StudentAccess> = listOf()

    var createdBy: Int? = null
    var modifiedBy: Int? = null

    override var createdAt: LocalDateTime? = null
    override var modifiedAt: LocalDateTime? = null

    infix fun from(advisory: Advisory): AdvisoryAccess {
        id = advisory.id
        classYear = advisory.classYear
        classLevel = advisory.classLevel
        advisoryName = advisory.advisoryName
        sectionName = advisory.sectionName
        sectionNumber = advisory.sectionNumber
        adviser = AdviserAccess() from advisory.adviser
        listStudentAccess = convertListStudentToAccess(advisory.listStudent)
        listStudentId = getLrnIDsFromListStr()

        return this
    }

    infix fun itsID(id : Int): AdvisoryAccess {
        this.id = id
        return this
    }

    fun toAdvisory(): Advisory {
        return Advisory(
            id,
            classYear,
            classLevel,
            advisoryName,
            sectionName,
            sectionNumber,
            adviser?.toAdviser() ?: throw IllegalArgumentException("Advisory Adiviser not initialized"),
            convertListStudentAccessToStudent(),
            getLrnIDsFromListStr()
        )
    }

    fun getLrnIDsFromListStr(): String {
        var ids = ""
        listStudentAccess.forEach { student->
            if (student.lrnID.isNotBlank()) {
               ids += "${student.lrnID},"
            }
        }
        return ids
    }

    fun convertListStudentAccessToStudent() =
        listStudentAccess.map { it.toStudent() }

    private fun convertListStudentToAccess(listStudent: List<Student>) =
        listStudent.map { StudentAccess() from it }

}

fun advisorySoftFieldSet() = listOf<Expression<*>>(
    AdvisoryDao.id,
    AdvisoryDao.advisoryName,
    AdvisoryDao.sectionName,
    AdvisoryDao.sectionNumber
)