package com.vhl.sms.caster.controller.web.school

import com.vhl.sms.caster.commons.errors.InvalidRequiredInputsError
import com.vhl.sms.caster.commons.errors.MissingRequiredInputsError
import com.vhl.sms.caster.commons.model.school.Advisory
import com.vhl.sms.caster.commons.payload.outgoing.AdvisoryJoinAdviserPrsn
import com.vhl.sms.caster.commons.payload.school.adviory.SearchAdvisoryP
import com.vhl.sms.caster.commons.response.Response
import com.vhl.sms.caster.features.validateToken
import com.vhl.sms.caster.service.school.AdvisoryService
import com.vhl.sms.caster.utils.StatusResponse
import getTokenModel
import io.ktor.application.call
import io.ktor.auth.authenticate
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.post

class AdvisoryController(
    private val advisoryService: AdvisoryService
) {
    fun createRoute(root: String, routing: Routing) {
        val base = "$root/school/advisory"
        routing {
            validateToken {
                authenticate {
                    saveAdvisory("$base/register")
                    updateAdvisory("$base/update")
                    searchAdvisory("$base/search")
                    findAdvisoryById("$base/search/{id}")
                }
            }
        }
    }

    private fun Route.saveAdvisory(url: String) {
        post(url) {
            val dataResponse: Response<Advisory>
            val advisory = call.receive<Advisory>()
            val tokenModel = call.request.getTokenModel()!!

            dataResponse = advisoryService.add(advisory, tokenModel.id) { code, message, dataValue ->
                Response(code, message, dataValue)
            }

            if (dataResponse.code != StatusResponse.SUCCESS) {
                call.response.status(HttpStatusCode.BadRequest)
            }

            call.respond(dataResponse)
        }
    }

    private fun Route.updateAdvisory(url: String) {
        post(url) {
            val dataResponse: Response<Advisory>
            val advisory = call.receive<Advisory>()
            val tokenModel = call.request.getTokenModel()!!

            dataResponse = advisoryService.update(advisory, tokenModel.id) { code, message, dataValue ->
                Response(code, message, dataValue)
            }

            if (dataResponse.code != StatusResponse.SUCCESS) {
                call.response.status(HttpStatusCode.BadRequest)
            }

            call.respond(dataResponse)
        }
    }

    private fun Route.searchAdvisory(url: String) {
        post(url) {
            val dataResponse: Response<List<AdvisoryJoinAdviserPrsn>>
            val searchCriteria = call.receive<SearchAdvisoryP>()

            dataResponse = advisoryService.findByCriteria(searchCriteria) { code, message, dataValue ->
                Response(code, message, dataValue)
            }

            if (dataResponse.code != StatusResponse.SUCCESS) {
                call.response.status(HttpStatusCode.BadRequest)
            }

            call.respond(dataResponse)
        }
    }

    private fun Route.findAdvisoryById(url: String) {
        get(url) {
            val dataResponse: Response<Advisory>
            try {
                val id = call.parameters["id"] ?: throw MissingRequiredInputsError("Missing Id", "ADVISORY_SC")
                dataResponse = advisoryService.findOneById(id.toInt()) { code, message, dataValue ->
                    Response(code, message, dataValue)
                }

                if (dataResponse.code != StatusResponse.SUCCESS) {
                    call.response.status(HttpStatusCode.BadRequest)
                }

                call.respond(dataResponse)
            } catch (e: NumberFormatException) {
                val error = InvalidRequiredInputsError("Invalid ID Format", "ADVISORY_SC")
                call.response.status(HttpStatusCode.BadRequest)
                call.respond(Response<String>(error.errorCode, error.message, null))
            }
        }
    }
}