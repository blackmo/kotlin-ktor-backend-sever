package com.vhl.sms.caster.utils.file

import com.github.doyaaaaaken.kotlincsv.client.CsvFileReader
import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.coroutines.yield
import java.io.File
import java.io.InputStream
import java.io.OutputStream

suspend fun InputStream.copyToSuspend(
    out: OutputStream,
    bufferSize: Int = DEFAULT_BUFFER_SIZE,
    yieldSize: Int = 4 * 1024 * 1024,
    dispatcher: CoroutineDispatcher = Dispatchers.IO
): Long {
    return withContext(dispatcher) {
        val buffer = ByteArray(bufferSize)
        var bytesCopied = 0L
        var bytesAfterYield = 0L
        while (true) {
            val bytes = read(buffer).takeIf { it >= 0 } ?: break
            out.write(buffer, 0, bytes)

            if (bytesAfterYield >= yieldSize) {
                yield()
                bytesAfterYield %= yieldSize
            }

            bytesCopied += bytes
            bytesAfterYield += bytes
        }

        return@withContext bytesCopied
    }
}

fun <T> validateReadCSVTemplate(templateFile: File, csvFile: File, action: CsvFileReader.() -> T): T? {
    var headersTemplate: List<String>? = null
    var dataReturn: T? = null
    csvReader().open(templateFile) {
        headersTemplate = readAllAsSequence().first()
    }

    csvReader().open(csvFile) {
        val fileHeader = readAllAsSequence().first()
        val equal = headersTemplate?.let { it equalWith fileHeader } ?: false

        if (!equal) {
            csvFile.delete()
            throw IllegalStateException("CSV header not Equal to given template")
        } else {
            csvFile.delete()
            dataReturn = action()
        }
    }
    return dataReturn
}

fun <T> validateReadCSVTemplate(template: InputStream, csvFile: File, action: CsvFileReader.() -> T): T? {
    var headersTemplate: List<String>? = null
    var dataReturn: T? = null
    csvReader().open(template) {
        headersTemplate = readAllAsSequence().first()
    }

    csvReader().open(csvFile) {
        val fileHeader = readAllAsSequence().first()
        val equal = headersTemplate?.let { it equalWith fileHeader } ?: false

        if (!equal) {
            csvFile.delete()
            throw IllegalStateException("CSV header not Equal to given template")
        } else {
            csvFile.delete()
            dataReturn = action()
        }
    }
    return dataReturn
}

infix fun List<String>.equalWith(with: List<String>): Boolean {
    return this.indices.none { this[it] != with[it] }
}


