package com.vhl.sms.caster.datasource.access.school.advisory

import com.vhl.sms.caster.commons.model.school.advisory.RefAdvisoryStudent
import com.vhl.sms.caster.datasource.access.UserDoa
import com.vhl.sms.caster.datasource.access.school.AdvisoryAccess
import com.vhl.sms.caster.datasource.access.school.AdvisoryDao
import com.vhl.sms.caster.datasource.access.school.PersonDao.references
import com.vhl.sms.caster.datasource.access.school.StudentAccess
import com.vhl.sms.caster.datasource.access.school.StudentDao
import com.vhl.sms.caster.exposed.Dao
import com.vhl.sms.caster.exposed.Domain
import java.time.LocalDateTime

object RefAdvisoryStudentDao: Dao("advisory_student") {
   val advisory = (integer("advisory_id") references AdvisoryDao.id)
   val student = integer("student_id") references StudentDao.id

   val createdBy = integer("created_by") references UserDoa.id
   val modifiedBy = integer("modified_by") references UserDoa.id
}

class RefAdvisoryStudentAccess: Domain {
   override var id: Int = 0
   override var createdAt: LocalDateTime? = null
   override var modifiedAt: LocalDateTime? = null

   var student: StudentAccess? = null
   var advisory: AdvisoryAccess? = null

   var createdBy: Int? = null
   var modifiedBy: Int? = null

   infix fun from (ref: RefAdvisoryStudent): RefAdvisoryStudentAccess {
      student =  StudentAccess() itsID ref.studentId
      advisory = AdvisoryAccess() itsID ref.advisoryId
      return this
   }

   infix fun itsID(id: Int): RefAdvisoryStudentAccess {
      this.id = id
      return this
   }

}