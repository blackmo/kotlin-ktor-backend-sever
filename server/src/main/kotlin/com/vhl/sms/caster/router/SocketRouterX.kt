package com.vhl.sms.caster.router

import com.beust.klaxon.Klaxon
import com.vhl.sms.caster.Logger
import com.vhl.sms.caster.commons.commands.WSConstants
import com.vhl.sms.caster.commons.errors.InvalidCredentialsError
import com.vhl.sms.caster.commons.errors.InvalidRequiredInputsError
import com.vhl.sms.caster.commons.errors.MissingRequiredInputsError
import com.vhl.sms.caster.commons.payload.common.WSCredential
import com.vhl.sms.caster.commons.response.Response
import com.vhl.sms.caster.datasource.access.UserAccess
import com.vhl.sms.caster.handler.SocketSessionHandler
import com.vhl.sms.caster.service.UserService
import com.vhl.sms.caster.utils.StatusResponse
import io.ktor.http.cio.websocket.*
import io.ktor.routing.Route
import io.ktor.sessions.get
import io.ktor.sessions.sessions
import io.ktor.util.KtorExperimentalAPI
import io.ktor.websocket.webSocket
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.consumeEach

@KtorExperimentalAPI
@ExperimentalCoroutinesApi
class SocketRouterX(
    private val webClientSocket: SocketSessionHandler,
    private val mobileClientSocket: SocketSessionHandler,
    private val userService: UserService
) {

    fun createWebSocket(root: String, routing: Route) {
        Logger.debug { "WebSocket :: creating web socket" }
        routing {

            val base = "/message/so"

            webSocket("$root$base/{type}") {

                val type = call.parameters["type"] ?: throw MissingRequiredInputsError(
                    "Missing Parameter",
                    "SOCKET_CONNECT"
                )
                val session = call.sessions.get<ChatSession>()
                val clientType = getParameterType(type)
                val socketSession = determineSocketType(clientType)

                if (session == null) {
                    Logger.debug { "WEB-SOCKET:: client session is null" }
                    close(CloseReason(CloseReason.Codes.VIOLATED_POLICY, "No Session"))
                    return@webSocket
                }

                consumeIncomingFrame(incoming, this, socketSession) {
                    Logger.debug { "client disconnected: ${session.id}" }
                }
            }

           launchSocketLifeStatusCheck()
        }
    }

    /**
     * launch live socket check in couroutine
     */
    private fun launchSocketLifeStatusCheck() {
        GlobalScope.launch { mobileClientSocket.checkLiveSocket() }
        GlobalScope.launch { webClientSocket.checkLiveSocket() }
    }

    /**
     * determine socket type
     */
    private fun determineSocketType(type: MessageSocketServer.ClientType): SocketSessionHandler = when (type) {
        MessageSocketServer.ClientType.DEVICE -> mobileClientSocket
        MessageSocketServer.ClientType.WEB ->
            webClientSocket
    }

    /**
     * determine parameter type given
     */
    private fun getParameterType(parameter: String): MessageSocketServer.ClientType {
        return when(parameter.toLowerCase()) {
            "web"->     MessageSocketServer.ClientType.WEB
            else ->     MessageSocketServer.ClientType.DEVICE
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    // receiving commands from a client
    //------------------------------------------------------------------------------------------------------------------
    private suspend fun receiveCommands(
        text: String,
        currentSocket: WebSocketSession,
        socketHandler: SocketSessionHandler
    ) {
        when {
            text.startsWith(WSConstants.LOGIN_WS_DEVICE) -> {
                val afterPrefix = text.removePrefix(WSConstants.LOGIN_WS_DEVICE)
                onLoginDevice(afterPrefix, socketHandler, currentSocket)
            }

            text.startsWith(WSConstants.LOGIN_WS_WEB) -> {
                val afterPrefix = text.removePrefix(WSConstants.LOGIN_WS_WEB)
                onLoginDevice(afterPrefix, socketHandler, currentSocket)
            }

            else -> currentSocket.close()
        }
    }

    private suspend fun onLoginDevice(
        commandText: String,
        socketHandler: SocketSessionHandler,
        currentSocket: WebSocketSession
    ) {
        try {
            val user = Klaxon()
                .parse<WSCredential>(commandText)
                ?: throw InvalidRequiredInputsError("Invalid Socket Input", "WS_SOCKET_LOGIN")

            if (user.deviceId.isNullOrBlank()) {
                throw InvalidRequiredInputsError("Invalid Socket Input Device ID", "WS_SOCKET_LOGIN")
            }

            validateLogin(user) { authorized ->
                if (!authorized) {
                    val error = InvalidCredentialsError("Invalid Login", "WS_SOCKET_LOGIN")
                    Logger.error { error }
                    currentSocket.close(error)
                } else {
                    socketHandler.addConnectedDeviceToUser(user.userName, user.deviceId!!, currentSocket)
                }
            }
        } catch (e: Exception) {
            currentSocket.send(e.message!!)
            currentSocket.close()
            Logger.error { e.message }

        }
    }

    /**
     *  consume incoming socket frames
     */
    private suspend fun consumeIncomingFrame(
        incoming: ReceiveChannel<Frame>,
        ws: DefaultWebSocketSession,
        wsSession: SocketSessionHandler,
        actionFinally: ()-> Unit
    ) {
        try {
            incoming.consumeEach { frame -> if (frame is Frame.Text) {
                    val text = frame.readText()
                    receiveCommands(text, ws, wsSession )
                }
            }
        } catch (e: Exception) {
            Logger.error { e }
        } finally {
            actionFinally()
        }
    }


    private inline fun validateLogin(user: WSCredential, action : (authorized: Boolean) -> Unit) {

        val userAccess = UserAccess().apply{
            userName = user.userName
            password = user.password
        }

        val response = userService.validateLogin(userAccess) { code, message, dataValue ->
            Response(code,message,dataValue)
        }

        when {
            response.code != StatusResponse.SUCCESS -> action(false)
            else -> action( true)
        }
    }

    data class ChatSession(val id: String, val expiration: Long, val user: String?  = null)
}