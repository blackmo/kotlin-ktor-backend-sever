package com.vhl.sms.caster.router

import com.vhl.sms.caster.Logger
import com.vhl.sms.caster.commons.errors.BasicErrors
import com.vhl.sms.caster.commons.errors.E_SESSION
import com.vhl.sms.caster.commons.payload.outgoing.WSMessage
import com.vhl.sms.caster.service.WebSocketService
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.WebSocketSession
import io.ktor.util.KtorExperimentalAPI
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.util.concurrent.ConcurrentHashMap

@KtorExperimentalAPI
@ExperimentalCoroutinesApi
class MessageSocketServer {

    private val connectedClient = ConcurrentHashMap<String, MutableList<WebSocketSession>>()
    private val wsMobileDevice  = WebSocketService(name = "ws-mobile-device")
    private val wsBrowser       = WebSocketService(name = "ws-mobile-browser")

    enum class ClientType { DEVICE, WEB }

    suspend fun connectDevice(device: String, socket: WebSocketSession) {
        wsMobileDevice.addLiveSocket(device, socket)
        notifySuccessFullyConnected(socket, ClientType.DEVICE)
    }

    suspend fun connectClient(client: String, socket: WebSocketSession) {
        wsBrowser.addLiveSocket(client, socket)
        notifySuccessFullyConnected(socket, ClientType.WEB)
    }

    suspend fun runChecking() {
        Logger.debug { "starting check live socket" }
        wsBrowser.removeDeadSocket()
//        wsMobileDevice.removeDeadSocket()
    }


    suspend fun sendUpdateToWebClient(update: String, clientId: String) {
        val value = Frame.Text(update)
        val listSockets = connectedClient[clientId]

        if (listSockets != null) {
            listSockets.forEach { it.send(value) }
        } else {
            throw BasicErrors("Connecting device not found", E_SESSION, "SOCKET_REQUEST")
        }
    }

    suspend fun sendCommandToDevice(command: WSMessage, id: String) {
        val value = Frame.Text(command.toString())
        val socket = wsMobileDevice.getSocket(id)

        if (socket != null) {
            socket.send(value)
        } else {
            throw BasicErrors("Connecting device not found", E_SESSION, "SOCKET_REQUEST")
        }
    }

    private suspend fun notifySuccessFullyConnected(socket: WebSocketSession, type: ClientType) {
        when (type) {
            ClientType.DEVICE -> {
                socket.send(Frame.Text("Device successfully connected"))
            }

            ClientType.WEB -> {
                socket.send(Frame.Text("Web Client successfully connected"))
            }
        }
    }


    private fun clientDeviceDisconnected(device: String) {
        wsMobileDevice.removeSocket(device)
    }

    private fun clientWebDisconnected(client: String) {
        wsBrowser.removeSocket(client)
    }
}
