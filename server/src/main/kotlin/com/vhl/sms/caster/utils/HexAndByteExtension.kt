package com.vhl.sms.caster.utils


fun ByteArray.toHexString()=this.joinToString(""){ String.format("%X",(it.toInt() and 0xFF)) }
fun String.byteArrayFromHexString()=this.chunked(2).map { it.toInt(16).toByte() }.toByteArray()

