package com.vhl.sms.caster.utils

class StatusResponse {
    companion object {
        const val SUCCESS = "SUCCESS"
        const val ERROR = "ERROR"
        const val FAILED = "FAILED"
        const val UNAUTHORIZED = "UNAUTHORIZED"
    }
}
