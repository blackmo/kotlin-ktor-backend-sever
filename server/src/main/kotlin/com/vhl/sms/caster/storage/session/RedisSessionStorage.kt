package com.vhl.sms.caster.storage.session

import com.vhl.sms.caster.storage.redis.Redis
import com.vhl.sms.caster.utils.byteArrayFromHexString
import com.vhl.sms.caster.utils.toHexString
import kotlinx.coroutines.future.await

class RedisSessionStorage(private val redisClient: Redis, private val timeOut: Long) : SimplifiedSessionStorage() {
    override suspend fun read(id: String): ByteArray? {
        val result = redisClient.async.get(id).await() ?: return null
        return result.byteArrayFromHexString()
    }

    override suspend fun write(id: String, data: ByteArray?) {
        if (data == null) {
            redisClient.async.del(id).await()
        } else {
            redisClient.async.set(id, data.toHexString())
            redisClient.async.expire(id, timeOut)
        }
    }
}