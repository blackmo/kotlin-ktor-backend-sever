package com.vhl.sms.caster.router

data class SocketLifeStatus(
    var socketId: String,
    var aliveAttached: Boolean = false,
    val commands: ArrayList<String> = arrayListOf(),
    var user: String? = null
)
