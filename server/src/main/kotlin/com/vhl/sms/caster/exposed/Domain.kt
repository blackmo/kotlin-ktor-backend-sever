package com.vhl.sms.caster.exposed

import java.time.LocalDateTime

/**
 *
 * @author Victor Harlan Lacson
 *
 */
interface Domain {
    var id: Int
    var createdAt : LocalDateTime?
    var modifiedAt: LocalDateTime?
}
