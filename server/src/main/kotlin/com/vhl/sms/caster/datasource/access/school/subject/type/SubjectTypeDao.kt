package com.vhl.sms.caster.datasource.access.school.subject.type

import com.vhl.sms.caster.commons.model.school.subject.type.SubjectType
import com.vhl.sms.caster.datasource.access.UserDoa
import com.vhl.sms.caster.exposed.Dao
import com.vhl.sms.caster.exposed.Domain
import java.time.LocalDateTime

object SubjectTypeDao : Dao("subject_type") {
    val name = varchar("type", 20)
    val description = varchar("description", 250)
    val gradeLevel = integer("gradle_level")

    val createdBy = integer("created_by").references(UserDoa.id).nullable()
    val modifiedBy = integer("modified_by").references(UserDoa.id).nullable()
}

class SubjectTypeAccess: Domain {
    override var id: Int = 0
    override var createdAt: LocalDateTime? = null
    override var modifiedAt: LocalDateTime? = null

    var name: String = ""
    var description = ""
    var gradeLevel = 0

    var createdBy: Int? = null
    var modifiedBy: Int? = null

    infix fun from(subjectType: SubjectType): SubjectTypeAccess {
        this.id = subjectType.id
        this.name = subjectType.name
        this.description = subjectType.description
        this.gradeLevel = subjectType.gradleLevel

        return this
    }

    infix fun itsId(id: Int): SubjectTypeAccess {
        this.id = id
        return this
    }
}