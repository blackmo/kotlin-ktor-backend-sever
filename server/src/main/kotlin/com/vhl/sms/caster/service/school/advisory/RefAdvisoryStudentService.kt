package com.vhl.sms.caster.service.school.advisory

import com.vhl.sms.caster.commons.model.school.Student
import com.vhl.sms.caster.commons.model.school.advisory.RefAdvisoryStudent
import com.vhl.sms.caster.commons.payload.outgoing.StudentJoinSoftPrsn
import com.vhl.sms.caster.commons.payload.school.student.SearchStudentP
import com.vhl.sms.caster.commons.response.Response
import com.vhl.sms.caster.datasource.access.school.advisory.RefAdvisoryStudentAccess
import com.vhl.sms.caster.repository.school.AdvisoryRepository
import com.vhl.sms.caster.repository.school.advisory.RefAdvisoryStudentRepository
import com.vhl.sms.caster.utils.StatusResponse
import com.vhl.sms.caster.utils.handler.error.responseCatch

class RefAdvisoryStudentService(
    private val refAdvisoryStudentRepository: RefAdvisoryStudentRepository,
    private val studentRepository: RefAdvisoryStudentRepository,
    private val advisoryRepository: AdvisoryRepository
) {
    fun addReference(
        reference: RefAdvisoryStudent,
        createdBy: Int,
        response: (code: String, message: String, dataValue: String?) -> Response<String?>
    ): Response<String?> {
        return responseCatch {
            val refAdccess = RefAdvisoryStudentAccess().from(reference).apply { this.createdBy = createdBy }
            refAdvisoryStudentRepository.addReference(refAdccess)
            response(StatusResponse.SUCCESS, "Successfully added Student to Advisory", null)
        }
    }

    fun deleteReference(
        reference: RefAdvisoryStudent,
        response: (code: String, message: String, dataValue: String?) -> Response<String?>
    ): Response<String?> {
        return responseCatch {
            refAdvisoryStudentRepository.deleteReference(RefAdvisoryStudentAccess() from reference)
            response(StatusResponse.SUCCESS, "Successfully Removed Student from Advisory", null)
        }
    }

    fun findStudentsOnAdvisory(
        advisoryId: Int,
        response: (code: String, message: String, dataValue: List<StudentJoinSoftPrsn>) -> Response<List<StudentJoinSoftPrsn>>
    ): Response<List<StudentJoinSoftPrsn>> {
        return responseCatch {
            val students = refAdvisoryStudentRepository.findStudentOnAdvisory(advisoryId)
            val dataResponse = students.map {
                StudentJoinSoftPrsn(
                    id = it.id,
                    lrnId = it.lrnID,
                    person = it.person!!.toPerson()
                )
            }

            response(StatusResponse.SUCCESS, "Successfully Retrieved Students", dataResponse)
        }
    }

    fun findStudentByCriteria(
        searchStudentP: SearchStudentP,
        response: (code: String, message: String, dataValue: List<Student>?) -> Response<List<Student>?>
    ): Response<List<Student>?> {
        return responseCatch {
            val dataResponse = refAdvisoryStudentRepository.findStudentsByCriteria(searchStudentP)
                .map { it.toStudent() }
            response(StatusResponse.SUCCESS, "Successfully Retrieved Students", dataResponse)
        }
    }
}