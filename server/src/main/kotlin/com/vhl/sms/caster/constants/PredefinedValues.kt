package com.vhl.sms.caster.constants

class PredefinedValues {
    companion object {
        const val DEFAULT_DATE_FORMAT = "MM-dd-yyyy hh:mm:ss"
    }
}