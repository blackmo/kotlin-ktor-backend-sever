package com.vhl.sms.caster.utils

import com.vhl.sms.caster.commons.response.Response
import com.vhl.sms.caster.service.TokenSessionService
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.delete
import io.ktor.routing.get
import io.ktor.routing.post

inline fun Route.deleteV(
   url: String,
   tokenSessionService: TokenSessionService,
   crossinline action: suspend ApplicationCall.() -> Unit
) {
   delete(url) {
      call.validateTokenSession(tokenSessionService) {
         action()
      }
   }
}

inline fun Route.postV(
   url: String,
   tokenSessionService: TokenSessionService,
   crossinline action : suspend ApplicationCall.() -> Unit) {
   this.post(url) {
      call.validateTokenSession(tokenSessionService) {
         action()
      }
   }
}


inline fun Route.getV(
   url: String,
   tokenSessionService: TokenSessionService,
   crossinline action : suspend ApplicationCall.() -> Unit) {
   get(url) {
      call.validateTokenSession(tokenSessionService) {
         action()
      }
   }
}

suspend inline fun ApplicationCall.validateTokenSession(
   tokenSessionService: TokenSessionService,
   action:  ApplicationCall.() -> Unit
) {
   val token = request.headers["Authorization"]!!.removePrefix("Bearer ")
   val activeToken = tokenSessionService.onLoggedUser(token)
   val invalidToken = tokenSessionService.onBlackListToken(token)

   when {
      !invalidToken && activeToken -> {
         action()
      }
      else -> {
         response.status(HttpStatusCode.Unauthorized)
         respond(Response<Any>(StatusResponse.UNAUTHORIZED, "Request Unauthorized"))
      }
   }
}

suspend inline fun ApplicationCall.validateToken(
   tokenSessionService: TokenSessionService,
   action: ApplicationCall.() -> Unit
) {
   val token = request.headers["Authorization"]!!.removePrefix("Bearer ")
   val activeToken = tokenSessionService.onLoggedUser(token)
   val invalidToken = tokenSessionService.onBlackListToken(token)

   if (invalidToken || !activeToken) {
      action()
      response.status(HttpStatusCode.Unauthorized)
      respond(Response<Any>(StatusResponse.UNAUTHORIZED, "Request Unauthorized"))
   }
}
