package com.vhl.sms.caster.repository.school.subject.type

import com.vhl.sms.caster.commons.errors.AlreadyExistError
import com.vhl.sms.caster.datasource.access.school.subject.type.SubjectTypeAccess
import com.vhl.sms.caster.datasource.access.school.subject.type.SubjectTypeDao
import com.vhl.sms.caster.exposed.ExposedSchema
import com.vhl.sms.caster.repository.DataRepository
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.UpdateBuilder
import org.jetbrains.exposed.sql.transactions.transaction

class SubjectTypeRepository:
    ExposedSchema<SubjectTypeAccess, SubjectTypeDao>(), DataRepository<SubjectTypeAccess> {

    override val entityName: String = "SubjectType"
    override val originName: String = "ORIGIN_SUBJECT_TYPE"

    override fun add(entity: SubjectTypeAccess): SubjectTypeAccess = transaction {
        SubjectTypeDao.checkIfAlreadyExist {
            (SubjectTypeDao.name eq entity.name) and (SubjectTypeDao.gradeLevel eq entity.gradeLevel)
        }.run {
            if (this) throw AlreadyExistError("Subject Type already exist", originName)
        }
        SubjectTypeDao.insertOne(entity)
    }

    override fun delete(entity: SubjectTypeAccess) = transaction {
        SubjectTypeDao.deleteWhere { SubjectTypeDao.id eq entity.id }
        commit()
    }

    override fun update(entity: SubjectTypeAccess): SubjectTypeAccess = transaction {
        SubjectTypeDao.updateOne(entity) {
            SubjectTypeDao.id eq entity.id
        }
    }

   fun findByNameAndGradleLevel(name: String, gradeLevel: Int?) {
       var query = SubjectTypeDao.select { SubjectTypeDao.name like  name }
       query =  gradeLevel?.let {
           query.andWhere { SubjectTypeDao.gradeLevel eq it }
               .groupBy(SubjectTypeDao.gradeLevel)
       } ?: query

   }

    override fun findOneById(id: Int): SubjectTypeAccess? = transaction {
        SubjectTypeDao.select { SubjectTypeDao.id eq id }
            .mapNotNull { it.toEntity() }.firstOrNull()
    }

    override fun findAllAndPaginate(count: Int, offset: Int): List<SubjectTypeAccess> = transaction {
        SubjectTypeDao.findAllAndPaginate(count, offset)
    }

    override fun UpdateBuilder<Any>.from(entity: SubjectTypeAccess, dao: SubjectTypeDao) {
        this[dao.id] = entity.id
        this[dao.name] = entity.name
        this[dao.description] = entity.description
        this[dao.gradeLevel] = entity.gradeLevel
    }

    override fun ResultRow.toEntity(): SubjectTypeAccess {
        val subjectTypeAccess = SubjectTypeAccess()

        subjectTypeAccess.id = this[SubjectTypeDao.id]
        subjectTypeAccess.name = this[SubjectTypeDao.name]
        subjectTypeAccess.gradeLevel = this[SubjectTypeDao.gradeLevel]

        subjectTypeAccess.createdAt = this[SubjectTypeDao.createdAt]
        subjectTypeAccess.modifiedAt = this[SubjectTypeDao.modifiedAt]

        return subjectTypeAccess
    }
}

