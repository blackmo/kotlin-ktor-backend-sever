package com.vhl.sms.caster.model

data class SimpleTokenModel(
    val sub: String,
    val iss: String,
    val id : Int,
    val exp: Long
)