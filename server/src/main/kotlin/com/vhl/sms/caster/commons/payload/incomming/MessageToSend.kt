package com.vhl.sms.caster.commons.payload.incomming

data class MessageToSend(val id: Int)