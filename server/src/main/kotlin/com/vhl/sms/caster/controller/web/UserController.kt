package com.vhl.sms.caster.controller.web

import com.vhl.sms.caster.Logger
import com.vhl.sms.caster.authentication.JwtConfig
import com.vhl.sms.caster.commons.model.User
import com.vhl.sms.caster.commons.model.enums.Privileges
import com.vhl.sms.caster.commons.payload.common.ChangePassword
import com.vhl.sms.caster.commons.response.Response
import com.vhl.sms.caster.datasource.access.UserAccess
import com.vhl.sms.caster.features.validateToken
import com.vhl.sms.caster.service.TokenSessionService
import com.vhl.sms.caster.service.UserService
import com.vhl.sms.caster.storage.redis.Redis
import com.vhl.sms.caster.utils.StatusResponse
import extractTokenModel
import getTokenModel
import io.ktor.application.call
import io.ktor.auth.authenticate
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.Routing
import io.ktor.routing.post

class UserController(
    private val userService: UserService,
    private val redis: Redis,
    private val tokenProvider: JwtConfig,
    private val tokenSessionService: TokenSessionService
) {
    init {
        val admin = User(0, "admin", "password_lnhs", null, Privileges.ADMIN)
        val response = userService.add(UserAccess() from admin, 0) { code, message, data ->
            Response(code, message, data)
        }

        when (response.code) {
            StatusResponse.SUCCESS -> {
                Logger.debug { "Default Admin Created" }
            }
            else -> {
                Logger.debug { "Default Admin Already Existed" }
            }
        }
    }

    private val base = "/web/user"
    fun createRoute(root: String, routing: Routing) {
        routing {
            register("$root$base/register")
            login("$root$base/login")
            validateToken {
                authenticate {
                    updatePassword("$root$base/update/password")
                    logout("$root$base/logout")
                }
            }
        }
    }

    private fun Route.register(url: String) {
        post(url) {
            val response: Response<UserAccess>

            val user = call.receive<User>()
            val userAccess = UserAccess() from user
            response = userService.add(userAccess, 0) { code, message, data ->
                Response(code, message, data)
            }

            if (response.code != StatusResponse.SUCCESS) {
                call.response.status(HttpStatusCode.Conflict)
                call.respond(response)
            } else {
                response.data
                    ?.toUser()
                    ?.run { setRedisPrivileged(this) }

                call.respond(Response<String>(response.code, response.message, null))
            }
        }
    }

    private fun Route.login(url: String) {
        post(url) {
            val user = call.receive<User>()
            var token: String? = null
            val userAccess = UserAccess() from user
            val response: Response<Any> = userService.validateLogin(userAccess) { code, message, dataValue ->
                Response(code, message, dataValue)
            }.let {
                it.data?.run {
                    val retrievedUser = User(id, userName, password, person?.toPerson(), privilege)
                    token = tokenSessionService.onLoggedUser(id)
                        ?: createAndSetToken(retrievedUser)
                }
                Response(it.code, it.message, token)
            }

            if (response.code != StatusResponse.SUCCESS) {
                call.response.status(HttpStatusCode.Unauthorized)
            }

            call.respond(response)
        }
    }

    private fun createAndSetToken(user: User): String {
        val token = tokenProvider.createToken(user)
        tokenSessionService.setLoggedInUser(user.id, token, tokenProvider.validityInMiliSeconds)
        return token
    }

    private fun Route.logout(url: String) {
        post(url) {
            val token = call.request.headers["Authorization"]!!.removePrefix("Bearer ")
            extractTokenModel(token)?.run {
                tokenSessionService.blackListToken(token, exp)
                tokenSessionService.removeActiveUser(token)
            }
            call.respond("Successful Logout")
        }
    }

    private fun Route.updatePassword(url: String) {
        post(url) {
            val password = call.receive<ChangePassword>()
            val tokenModel = call.request.getTokenModel()!!

            val response = userService.updatePassword(password, tokenModel.id) { code, message, dataValue ->
                Response(code, message, dataValue)
            }

            if (response.code != StatusResponse.SUCCESS) {
                call.response.status(HttpStatusCode.BadRequest)
            }
            call.respond(response)
        }
    }

    private fun setRedisPrivileged(user: User) {
        val id = "privilege::${user.id}"
        redis.async.set(id, user.privileges.name)
    }
}