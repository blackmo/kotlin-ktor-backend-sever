package com.vhl.sms.caster.utils

import com.vhl.sms.caster.Logger
import com.vhl.sms.caster.commons.errors.InvalidTokenError

class CredentialValidationUtils {
    companion object {

        inline fun validateToken(token: String?, route: String,  action: () -> Unit) {
            if (token.isNullOrEmpty()) {
                Logger.error { "route: $route || illegal access parameter invalid session id" }
                throw InvalidTokenError(origin = "BROWSER_REQUEST")
            }

            //todo - validate token id here
            action()
        }
    }
}