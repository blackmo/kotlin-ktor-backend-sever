package com.vhl.sms.caster.controller.web.school

import com.vhl.sms.caster.commons.model.school.Adviser
import com.vhl.sms.caster.commons.payload.school.adviser.AdviserSearchP
import com.vhl.sms.caster.commons.payload.school.person.FPersonByName
import com.vhl.sms.caster.commons.response.Response
import com.vhl.sms.caster.features.validateToken
import com.vhl.sms.caster.service.school.AdviserService
import com.vhl.sms.caster.utils.StatusResponse
import getTokenModel
import io.ktor.application.call
import io.ktor.auth.authenticate
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.post

class AdviserController(
    private val adviserService: AdviserService
) {
    fun createRoute(root: String, routing: Routing) {
        val base = "$root/school/adviser"
        routing {
            validateToken {
                authenticate {
                    saveAdviser("$base/register")
                    searchByLastName("$base/search")
                    updateAdviser("$base/update")
                    getAllAdvisers("$base/get/all")
                }
            }
        }
    }

    private fun Route.saveAdviser(url: String) {
        post(url) {
            val dataResponse: Response<Adviser>
            val adviser = call.receive<Adviser>()
            val tokenModel = call.request.getTokenModel()!!
            dataResponse = adviserService.add(adviser, tokenModel.id) { code, message, dataValue ->
                Response(code, message, dataValue)
            }

            if (dataResponse.code != StatusResponse.SUCCESS) {
                call.response.status(HttpStatusCode.BadRequest)
            }

            call.respond(dataResponse)
        }
    }

    private fun Route.searchByLastName(url: String) {
        post(url) {
            val dataResponse: Response<List<Adviser>>
            val searchCriteria = call.receive<AdviserSearchP>()
            val fPerson = FPersonByName(searchCriteria.lastName)

            dataResponse = adviserService.findOneEagerLike(fPerson, false) { code, message, dataValue ->
                Response(code, message, dataValue)
            }

            if (dataResponse.code != StatusResponse.SUCCESS) {
                call.response.status(HttpStatusCode.BadRequest)
            }

            call.respond(dataResponse)
        }
    }

    private fun Route.updateAdviser(url: String) {
        post(url) {
            val dataResponse: Response<Adviser>
            val adviser = call.receive<Adviser>()
            val tokenModel = call.request.getTokenModel()!!

            dataResponse = adviserService.update(adviser, tokenModel.id) { code, message, dataValue ->
                Response(code, message, dataValue)
            }

            if (dataResponse.code != StatusResponse.SUCCESS) {
                call.response.status(HttpStatusCode.BadRequest)
            }

            call.respond(dataResponse)
        }
    }

    private fun Route.getAllAdvisers(url: String) {
        get(url) {
            val dataResponse: Response<List<Adviser>>

            dataResponse = adviserService.getAllAdviser { code, message, dataValue ->
                Response(code, message, dataValue)
            }

            if (dataResponse.code != StatusResponse.SUCCESS) {
                call.response.status(HttpStatusCode.BadRequest)
            }

            call.respond(dataResponse)
        }
    }
}