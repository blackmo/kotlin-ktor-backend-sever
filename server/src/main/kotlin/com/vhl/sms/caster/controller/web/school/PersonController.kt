package com.vhl.sms.caster.controller.web.school

import com.vhl.sms.caster.commons.model.school.Person
import com.vhl.sms.caster.commons.payload.school.person.FPersonByName
import com.vhl.sms.caster.commons.response.Response
import com.vhl.sms.caster.features.validateToken
import com.vhl.sms.caster.service.school.PersonService
import com.vhl.sms.caster.utils.StatusResponse
import getTokenModel
import io.ktor.application.call
import io.ktor.auth.authenticate
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.post

class PersonController(
    private val personService: PersonService
) {

    fun createRoute(root: String, routing: Routing) {
        val base = "$root/school/person"

        routing {
            validateToken {
                authenticate {
                    savePerson("$base/save")
                    updatePerson("$base/update")
                }
            }
        }
    }

    private fun Route.savePerson(url: String) {
        post(url) {
            val dataResponse: Response<Person>
            val person = call.receive<Person>()
            val tokenModel = call.request.getTokenModel()!!


            dataResponse = personService.add(person, tokenModel.id) { code, message, dataValue ->
                Response(code, message, dataValue)
            }

            if (dataResponse.code != StatusResponse.SUCCESS) {
                call.response.status(HttpStatusCode.BadRequest)
            }
            call.respond(dataResponse)
        }
    }

    private fun Route.updatePerson(url: String) {
        post(url) {
            val dataResponse: Response<Person>
            val person = call.receive<Person>()
            val tokenModel = call.request.getTokenModel()!!

            dataResponse = personService.update(person, tokenModel.id) { code, message, dataValue ->
                Response(code, message, dataValue)
            }

            if (dataResponse.code != StatusResponse.SUCCESS) {
                call.response.status(HttpStatusCode.BadRequest)
            }
            call.respond(dataResponse)
        }
    }

    private fun Route.findPersonByName(url: String) {
        get(url) {
            val dataResponse: Response<List<Person>>
            val payload = call.receive<FPersonByName>()

            dataResponse = personService.findByName(payload) { code, message, dataValue ->
                Response(code, message, dataValue)
            }

            if (dataResponse.code != StatusResponse.SUCCESS) {
                call.response.status(HttpStatusCode.BadRequest)
            }

            call.respond(dataResponse)
        }
    }
}
