package com.vhl.sms.caster.commons.payload.incomming

data class MessageToUpdate(
    val id: Int,
    val status: String
)