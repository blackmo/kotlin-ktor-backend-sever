package com.vhl.sms.caster.repository

import com.vhl.sms.caster.commons.errors.AlreadyExistError
import com.vhl.sms.caster.datasource.access.UserAccess
import com.vhl.sms.caster.datasource.access.UserDoa
import com.vhl.sms.caster.datasource.access.school.PersonAccess
import com.vhl.sms.caster.exposed.ExposedSchema
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.statements.UpdateBuilder
import org.jetbrains.exposed.sql.transactions.transaction

class UserRepository: ExposedSchema<UserAccess, UserDoa>(), DataRepository<UserAccess> {
    override val entityName: String = "Message"
    override val originName: String = "ORIGIN_USER"

    override fun add(entity: UserAccess): UserAccess = transaction {
        UserDoa
            .checkIfAlreadyExist { UserDoa.userName eq entity.userName }
            .run {
                if (this) throw AlreadyExistError("Message entity duplicate ", "${originName}_SAVE")
            }
        UserDoa.insertOne(entity)
    }

    override fun delete(entity: UserAccess) = transaction {
        UserDoa.deleteWhere { UserDoa.id eq entity.id }
        commit()
    }

    override fun update(entity: UserAccess): UserAccess = transaction {
        UserDoa.updateOne(entity) {
            UserDoa.id eq entity.id
        }
    }

    override fun findOneById(id: Int): UserAccess? = transaction {
        UserDoa.select { UserDoa.id eq id }.map { it.toEntity() }.firstOrNull()
    }

    override fun findAllAndPaginate(count: Int, offset: Int): List<UserAccess> = transaction {
        UserDoa.findAllAndPaginate(count, offset)
    }

    fun updatePassword(password: String, userId: Int) = transaction {
        UserDoa.updateOne({ UserDoa.id eq userId }) {
            it[this.password] = password
        }
    }

    override fun UpdateBuilder<Any>.from(entity: UserAccess, dao: UserDoa) {
        this[dao.id] = entity.id
        this[dao.password] = entity.password
        this[dao.userName] = entity.userName
        this[dao.person] = entity.person?.id
        this[dao.privilege] = entity.privilege
    }

    override fun ResultRow.toEntity(): UserAccess {
        val userAccess = UserAccess()
        userAccess.id = this[UserDoa.id]
        userAccess.userName = this[UserDoa.userName]
        userAccess.password = this[UserDoa.password]
        userAccess.person = personNullOrHasId(this[UserDoa.person])

        return userAccess
    }

    fun findOneByUserName(userName: String) = transaction {
        UserDoa.select { UserDoa.userName eq userName }.mapNotNull { it.toEntity() }.firstOrNull()
    }

    private fun personNullOrHasId(id: Int?): PersonAccess? = when {
        id != null -> PersonAccess() itsId id
        else -> null
    }
}