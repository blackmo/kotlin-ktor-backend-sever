package com.vhl.sms.caster.service

import com.vhl.sms.caster.commons.payload.WSCODe
import com.vhl.sms.caster.commons.payload.outgoing.WSMessage
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.WebSocketSession
import io.ktor.util.KtorExperimentalAPI
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import java.util.concurrent.ConcurrentHashMap

@ExperimentalCoroutinesApi
@KtorExperimentalAPI
class WebSocketService(val name: String = "Default-Web-Socket-Service", private val delayTime: Long = 300) {
    private val liveSockets = ConcurrentHashMap<String, WebSocketSession>()
    var isChecking: Boolean = true

    fun addLiveSocket(key: String, socket: WebSocketSession) {
        liveSockets[key] = socket
    }

    private val ping = WSMessage(code = WSCODe.PING, message = WSCODe.PING)

    fun getSocket(key: String) = liveSockets[key]
    fun keyExists(key: String) = liveSockets.containsKey(key)
    fun removeSocket(key: String) {
        liveSockets.remove(key)
    }

    suspend fun removeDeadSocket() {
        while (isChecking) {
            delay(3000)
            liveSockets.forEach {(key, socket) ->
                delay(3000)
//                println("key: $key, status: ${socket.outgoing.isClosedForSend}")
                performSocketCheck(key, socket)
            }
        }
    }

    private suspend fun performSocketCheck(key: String, socket: WebSocketSession) {
        runCatching {
            if (!socket.outgoing.isClosedForSend) {
                socket.send(Frame.Text(ping.toString()))
            } else {
                socket.send(Frame.Text(ping.toString()))
                liveSockets.remove(key)
            }
        }.onFailure {
            liveSockets.remove(key)
        }
    }
}