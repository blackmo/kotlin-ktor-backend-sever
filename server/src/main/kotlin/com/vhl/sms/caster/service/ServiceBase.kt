package com.vhl.sms.caster.service

import com.vhl.sms.caster.commons.response.Response

interface ServiceBase<T> {
    fun add(
        entity: T,
        createdBy: Int,
        response: (code: String, message: String, dataValue: T?) -> Response<T>
    ): Response<T>

    fun delete(
        entity: T,
        response: (code: String, message: String, dataValue: T?) -> Response<T>
    ): Response<T>
    fun update(
        entity: T,
        modifiedBy: Int,
        response: (code: String, message: String, dataValue: T?) -> Response<T>
    ): Response<T>

    fun findOneById(
        id: Int,
        response: (code: String, message: String, dataValue: T?) -> Response<T>
        ): Response<T>
    fun findAllAngPaginate(
        count: Int,
        offset: Int,
        response: (code: String, message: String, dataValue: List<T>?) -> Response<List<T>>
    ): Response<List<T>>
}