package com.vhl.sms.caster.constants
class ModuleTags {
    companion object {
        const val WS_DEVICE = "SOCKET-MOBILE"
        const val WS_WEB = "SOCKET-WEB"

        const val SERVER_ENCRYPTION  = "SERVER-ENCRYPTION"

        const val REDIS_DB1 = "REDIS-DB1"
        const val REDIS_DB2 = "REDIS-DB2"

        const val UPDATE_CLIENT_WS_WEB = "UPDATE_CLIENT_WS_WEB"
        const val UPDATE_CLIENT_WS_DEVICE = "UPDATE_CLIENT_WS_DV"
    }
}