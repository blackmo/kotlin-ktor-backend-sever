package com.vhl.sms.caster.exposed

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.`java-time`.datetime
import org.jetbrains.exposed.sql.statements.InsertStatement
import org.jetbrains.exposed.sql.statements.UpdateBuilder
import org.jetbrains.exposed.sql.statements.UpdateStatement
import java.time.LocalDateTime

/**
 * Data Source class implementing bare bones sql dsl of JetBrains Exposed
 * @author Victor Harlan Lacson
 * @param T - Entity Class
 * @param D - Dao object representing T
 * @property entityName - entity name of the inheriting sub class
 * @property originName - marker string
 */
abstract class ExposedSchema<T : Domain, D: Dao> {

    protected abstract val entityName: String
    protected abstract val originName: String

    /**
     * Checks whether an entity in database already exists
     * @param where - where clause
     * @return  a boolean "true" if exist in 1 or more otherwise "false"
     */
    protected fun Dao.checkIfAlreadyExist(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean =
        !this.select(where).mapNotNull { it.let { createdAt } }.isNullOrEmpty()

    /**
     * @param entity - entity object to be inserted
     * @param extraInsert - additional insert statement
     */
    protected fun D.insertOne(entity: T, extraInsert: (InsertStatement<Number>.() -> Unit)? = null): T {
        val now = LocalDateTime.now()
        return insert {
            it.from(entity, this)
            it[createdAt] = now
            it[modifiedAt] = now

            if (extraInsert != null) {
                extraInsert(it)
            }
        }.let {
            entity.id = it[this.id]
            entity.createdOn(now)
        }
    }

    /**
     * Updates database by given entity according to where clause given
     * @param entity - The Entity Object to be updated
     * @param extraUpdate - An additional Update statement
     * @param where - where clause
     * @return  updated entity object
     */
    protected fun D.updateOne(
        entity: T,
        extraUpdate: (UpdateStatement.() -> Unit)? = null,
        where: SqlExpressionBuilder.() -> Op<Boolean>
    ): T {
        val now = LocalDateTime.now()
        return this.updateExactlyOne(where) {
            it.from(entity, this)
            it[modifiedAt] = now

            if (extraUpdate != null) {
                extraUpdate(it)
            }

        }.run {
            entity.modifiedOn(now)
        }
    }

    /**
     * Updates database by given entity according to where clause given
     * @param extraUpdate - An additional Update statement
     * @param where - where clause
     * @return  updated entity object
     */
    protected fun D.updateOne(
        where: SqlExpressionBuilder.() -> Op<Boolean>,
        extraUpdate: (UpdateStatement.() -> Unit)? = null,
        body: D.(UpdateStatement) -> Unit
    ) {
        this.updateExactlyOne(where) {
            if (extraUpdate != null) {
                extraUpdate(it)
            }
            body(it)
        }
    }


    /**
     * Pagination method in find all
     * @param count - number of entities to be returned
     * @param offset - starting index
     */
    protected fun D.findAllAndPaginate(count: Int, offset: Int): List<T> =
        this.selectAll().limit(count, offset.toLong()).map { it.toEntity() }

    private fun D.updateExactlyOne(where: SqlExpressionBuilder.() -> Op<Boolean>, body: D.(UpdateStatement) -> Unit) =
        update(where = where, body = body).also {
            check(it == 1) { "Expected to updateOne exactly one row, but was: $it" }
        }

    /**
     * Abstract method to be implemented by subclass so an updateOne builder can be created for insert and updateOne queries
     * for data access object
     * @param entity - entity object
     * @param dao - data access object
     * @sample  {
     *  this[[dao.id]] = [entity.id]
     *  ....
     * }
     */
    protected abstract fun UpdateBuilder<Any>.from(entity: T, dao: D)

    /**
     * Converts a results from query statement to Specified entity
     * @param dao - the data access object represent a result from database
     * @return the specified entity object
     * @sample
     *  {
     *    return Animal(
     *   name       = this[dao.name]
     *   habitat    = this[dao.habitat]
     *   ....
     *   )
     *  }
     */
    protected abstract fun ResultRow.toEntity(): T

    /**
     * Converts an exposed query result to data class represented by this repository
     */
    fun resultToEntity(result: ResultRow): T = result.toEntity()

    private fun T.createdOn(now: LocalDateTime): T {
        this.modifiedAt = now
        this.createdAt = now
        return this
    }

    private fun T.modifiedOn(now: LocalDateTime): T {
        this.modifiedAt = now
        return this
    }

}

open class Dao(name: String): Table(name) {
    val id = integer("${name}_id").autoIncrement()
    val createdAt = datetime("created_at")
    val modifiedAt = datetime("modified_at")
    override val primaryKey = PrimaryKey(id)
}
