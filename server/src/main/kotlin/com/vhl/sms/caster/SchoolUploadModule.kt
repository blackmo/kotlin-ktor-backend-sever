package com.vhl.sms.caster

import com.vhl.sms.caster.controller.web.school.UploadController
import io.ktor.application.Application
import io.ktor.routing.routing
import org.kodein.di.generic.instance


fun Application.uploadModule() {

    val kodein = KodeinInstance.kodein ?: throw IllegalStateException("Kodein DI not intialized")
    val uploadController: UploadController by kodein.instance<UploadController>()

    routing {
        uploadController.createRoute("home", this)
    }
}