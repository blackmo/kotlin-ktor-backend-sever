package com.vhl.sms.caster.database

import com.vhl.sms.caster.datasource.access.ContactDao
import com.vhl.sms.caster.datasource.access.DeviceDao
import com.vhl.sms.caster.datasource.access.MessageDao
import com.vhl.sms.caster.datasource.access.UserDoa
import com.vhl.sms.caster.datasource.access.school.*
import com.vhl.sms.caster.datasource.access.school.advisory.RefAdvisoryStudentDao
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import getProperty
import io.ktor.application.ApplicationEnvironment
import io.ktor.util.KtorExperimentalAPI
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction

@KtorExperimentalAPI
class DBSource(private val env : ApplicationEnvironment) {

    companion object {
        const val MYSQL_DB = "MYSQL_DB"
        const val DEFAULT = "DEFAULT"
    }

    fun initDataBaseConnection(db: String) {
       val dbSource = when(db) {
           MYSQL_DB -> hikariConfigMysql()
           else -> defaultDbSource()
       }
        Database.connect(dbSource)
    }

    fun createSchema() {
        transaction {
            SchemaUtils.create(
                UserDoa, ContactDao, MessageDao, DeviceDao, AdviserDao,
                AdvisoryDao, PersonDao, StudentDao, SubjectDao, RefAdvisoryStudentDao
            )
        }
    }

     private fun hikariConfigMysql(): HikariDataSource {
         val config = HikariConfig()
         config.jdbcUrl = env.getProperty("database.jbdcUrl")
         config.driverClassName = "com.mysql.cj.jdbc.Driver"
         config.username = env.getProperty("database.user")
         config.password = env.getProperty("database.password")

         return HikariDataSource(config)
     }

    private fun defaultDbSource(): HikariDataSource {
        val config = HikariConfig()
        config.jdbcUrl =  env.getProperty("database.jbdcUrl")
        config.driverClassName = "com.mysql.cj.jdbc.Driver"
        config.username = env.getProperty("database.user")
        config.password = env.getProperty("database.password")

        return HikariDataSource(config)
    }
}