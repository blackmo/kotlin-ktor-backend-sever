package com.vhl.sms.caster.di.module

//import com.vhl.sms.caster.controller.web.WebContactController
import com.vhl.sms.caster.commons.service.EncryptionService
import com.vhl.sms.caster.constants.ModuleTags
import com.vhl.sms.caster.controller.web.UserController
import com.vhl.sms.caster.handler.SocketSessionHandler
import com.vhl.sms.caster.repository.UserRepository
import com.vhl.sms.caster.router.SocketRouterX
import com.vhl.sms.caster.service.UpdateClientService
import com.vhl.sms.caster.service.UserService
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class ServerModule {
    companion object {

        fun userModule() = Kodein.Module("user-modules") {
            bind() from provider { UserRepository() }
            bind() from provider { UserService(instance(), instance(tag = ModuleTags.SERVER_ENCRYPTION)) }
            bind() from provider {
                UserController(
                    instance(),
                    instance(ModuleTags.REDIS_DB1),
                    instance(),
                    instance() // from session-module
                )
            }
        }


        fun webSocketModule() = Kodein.Module("repo-web-socket") {
            bind(tag = ModuleTags.WS_WEB) from singleton { SocketSessionHandler() }
            bind(tag = ModuleTags.WS_DEVICE) from singleton { SocketSessionHandler() }
            bind() from singleton {
                SocketRouterX(
                    instance(ModuleTags.WS_WEB),
                    instance(ModuleTags.WS_DEVICE),
                    instance()
                )
            }
        }


        fun serverEncryption(password: String, salt: String) = Kodein.Module("server-encryption") {
            bind(tag = ModuleTags.SERVER_ENCRYPTION) from provider {
                EncryptionService(password, salt)
            }
        }

        fun updateDeviceSocketService() = Kodein.Module("update-socket-service") {
            bind(tag = ModuleTags.UPDATE_CLIENT_WS_DEVICE) from singleton {
                UpdateClientService(instance(tag = ModuleTags.WS_DEVICE))
            }

            bind(tag = ModuleTags.UPDATE_CLIENT_WS_WEB) from singleton {
                UpdateClientService(instance(tag = ModuleTags.WS_WEB))
            }
        }
    }
}
