package com.vhl.sms.caster.controller.web.school

import com.vhl.sms.caster.commons.errors.MissingRequiredInputsError
import com.vhl.sms.caster.commons.payload.school.student.batch.BRegistrationResponse
import com.vhl.sms.caster.commons.response.Response
import com.vhl.sms.caster.features.validateToken
import com.vhl.sms.caster.service.upload.UploadService
import com.vhl.sms.caster.utils.StatusResponse
import com.vhl.sms.caster.utils.file.copyToSuspend
import getTokenModel
import io.ktor.application.call
import io.ktor.auth.authenticate
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.PartData
import io.ktor.http.content.forEachPart
import io.ktor.http.content.streamProvider
import io.ktor.request.receiveMultipart
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.Routing
import io.ktor.routing.post
import java.io.File

class UploadController(
    private val uploadService: UploadService,
    private val uploadDestination: String,
    private val namingConvention: String
) {
    fun createRoute(root: String, routing: Routing) {
        val base = "$root/school/upload"

        routing {
            validateToken {
                authenticate {
                    uploadCSVStudentRegistration("$base/register-csv")
                }
            }
        }
    }

    private fun Route.uploadCSVStudentRegistration(url: String) {
        post(url) {
            var dataResult: BRegistrationResponse? = null
            val tokenModel = call.request.getTokenModel()!!

            try {
                val type = call.request.headers["type"] ?: throw MissingRequiredInputsError(
                    "Header entry \"type\" not found",
                    "UPLD_CNTRL"
                )
                val disposition = call.request.headers["disposition"]
                    ?: throw MissingRequiredInputsError("Header entry \"disposition\" not found", "UPLD_CNTRL")
                val year = call.request.headers["year"] ?: throw MissingRequiredInputsError(
                    "Header entry \"year\" not found",
                    "UPLD_CNTR"
                )
                val section = call.request.headers["section"] ?: throw MissingRequiredInputsError(
                    "Header entry \"section\" not found",
                    "UPLD_CNTR"
                )

                val multipart = call.receiveMultipart()

                val destination = "$uploadDestination/$disposition/$type/$year-$section"

                File(destination).mkdirs()

                multipart.forEachPart { part ->
                    when (part) {
                        is PartData.FileItem -> {
                            val ext = File(part.originalFileName).extension
                            val file = File(
                                destination,
                                "$namingConvention-${System.currentTimeMillis()}.$ext"
                            )
                            part.streamProvider()
                                .use { its -> file.outputStream().buffered().use { its.copyToSuspend(it) } }
                            dataResult = uploadService.receiveStudentBatchUpload(file, tokenModel.id)
                        }
                    }
                    part.dispose()
                }
                call.respond(Response(StatusResponse.SUCCESS, "Batch Upload Executed", dataResult))
            } catch (e: Exception) {
                this.call.response.status(HttpStatusCode.BadRequest)
                call.respond(Response<BRegistrationResponse>(StatusResponse.FAILED, e.message!!, null))
            }
        }
    }
}

