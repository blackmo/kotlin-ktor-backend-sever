package com.vhl.sms.caster.service.school

import com.vhl.sms.caster.Logger
import com.vhl.sms.caster.commons.errors.BasicErrors
import com.vhl.sms.caster.commons.model.school.Person
import com.vhl.sms.caster.commons.payload.school.person.FPersonByName
import com.vhl.sms.caster.commons.response.Response
import com.vhl.sms.caster.datasource.access.school.PersonAccess
import com.vhl.sms.caster.repository.school.PersonRepository
import com.vhl.sms.caster.service.ServiceBase
import com.vhl.sms.caster.utils.StatusResponse
import com.vhl.sms.caster.utils.handler.error.responseCatch

class PersonService(
    private val personRepository: PersonRepository
): ServiceBase<Person>{
    override fun add(
        entity: Person,
        createdBy: Int,
        response: (code: String, message: String, dataValue: Person?) -> Response<Person>
    ): Response<Person> {
        return try {
            val personAccess = PersonAccess().from(entity).apply { this.createdBy = createdBy }
            val value = personRepository.add(personAccess)
            response(StatusResponse.SUCCESS, "Person Successfully Saved", value.toPerson())
        } catch (e: BasicErrors) {
            Logger.error { e }
            response(e.errorCode, e.message, null)
        }
    }

    override fun delete(
        entity: Person,
        response: (code: String, message: String, dataValue: Person?) -> Response<Person>
    ): Response<Person> {
        return try {
            personRepository.delete(PersonAccess() from entity)
            response(StatusResponse.SUCCESS, "Successfully deleted person", entity)
        } catch (e: BasicErrors) {
            Logger.error { e }
            response(e.errorCode, e.message, null)
        } catch (e: Exception) {
            Logger.error { e.message!! }
            response(StatusResponse.ERROR, e.message!!, entity)
        }
    }

    override fun update(
        entity: Person,
        modifiedBy: Int,
        response: (code: String, message: String, dataValue: Person?) -> Response<Person>
    ): Response<Person> {
        return try {
            val personAccess = PersonAccess().from(entity).apply { this.modifiedBy = modifiedBy }
            personRepository.update(personAccess)
            response(StatusResponse.SUCCESS, "Successfully updated person", entity)
        } catch (e: BasicErrors) {
            Logger.error { e }
            response(e.errorCode, e.message, null)
        } catch (e: Exception) {
            Logger.error { e.message!! }
            response(StatusResponse.ERROR, e.message!!, entity)
        }
    }

    override fun findOneById(
        id: Int,
        response: (code: String, message: String, dataValue: Person?) -> Response<Person>
    ): Response<Person> {
        return try {
            val value = personRepository.findOneById(id)
            response(StatusResponse.SUCCESS, "Successfully to retrieved person", value?.toPerson())
        } catch (e: BasicErrors) {
            Logger.error { e }
            response(e.errorCode, e.message, null)
        } catch (e: Exception) {
            Logger.error { e.message!! }
            response(StatusResponse.ERROR, e.message!!, null)
        }
    }

    override fun findAllAngPaginate(
        count: Int,
        offset: Int,
        response: (code: String, message: String, dataValue: List<Person>?) -> Response<List<Person>>
    ): Response<List<Person>> {
        return try {
            val value = personRepository.findAllAndPaginate(count, offset).map { it.toPerson() }
            response(StatusResponse.SUCCESS, "Successfully to retrieved persons", value)
        } catch (e: BasicErrors) {
            Logger.error { e }
            response(e.errorCode, e.message, null)
        } catch (e: Exception) {
            Logger.error { e.message!! }
            response(StatusResponse.ERROR, e.message!!, null)
        }
    }

    fun addMultiple(
        listPerson: List<Person>,
        response: (code: String, message: String, dataValue: List<Person>?) -> Response<List<Person>>
    ): Response<List<Person>> {
        return try {
            val value = personRepository.addMultiple(listPerson.map { PersonAccess() from it })
                .map { it.toPerson() }
            response(StatusResponse.SUCCESS, "Successfully added persons", value)
        } catch (e: BasicErrors) {
            Logger.error { e }
            response(e.errorCode, e.message, null)
        } catch (e: Exception) {
            Logger.error { e.message!! }
            response(StatusResponse.ERROR, e.message!!, null)
        }
    }

    fun findByName(
        payload: FPersonByName,
        response: (code: String, message: String, dataValue: List<Person>?) -> Response<List<Person>>
    ): Response<List<Person>> {
        return try {
            val value = personRepository.findByName(
                payload.lastName,
                payload.firstName,
                payload.middleName,
                payload.count,
                payload.offset
            ).map { it.toPerson() }
            response(StatusResponse.SUCCESS, "Search Success", value)
        } catch (e: BasicErrors) {
            Logger.error { e }
            response(e.errorCode, e.message, null)
        } catch (e: Exception) {
            Logger.error { e.message!! }
            response(StatusResponse.ERROR, e.message!!, null)
        }
    }

    fun addOrGet(
        person: PersonAccess,
        response: (code: String, message: String, dataValue: Person) -> Response<Person>
    ) = responseCatch {
        val data = personRepository.addOrGet(person).toPerson()
        response(StatusResponse.SUCCESS, "Person is Saved", data)
    }
}