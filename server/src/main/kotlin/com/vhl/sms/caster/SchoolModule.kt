package com.vhl.sms.caster

import com.vhl.sms.caster.controller.web.school.AdviserController
import com.vhl.sms.caster.controller.web.school.AdvisoryController
import com.vhl.sms.caster.controller.web.school.RefAdvisoryStudentController
import com.vhl.sms.caster.controller.web.school.StudentController
import io.ktor.application.Application
import io.ktor.routing.routing
import org.kodein.di.generic.instance

fun Application.schoolRouteModule() {
    val kodein = KodeinInstance.kodein ?: throw IllegalStateException("Kodein DI not intialized")
    val studentController: StudentController by kodein.instance<StudentController>()
    val adviserController: AdviserController by kodein.instance<AdviserController>()
    val advisoryController: AdvisoryController by kodein.instance<AdvisoryController>()
    val refAdvisory: RefAdvisoryStudentController by kodein.instance<RefAdvisoryStudentController>()

    routing {
        adviserController.createRoute("home", this)
        advisoryController.createRoute("home", this)
        studentController.createRoute("home", this)
        refAdvisory.createRoute("home", this)
    }
}