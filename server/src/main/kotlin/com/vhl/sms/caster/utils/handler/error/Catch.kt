package com.vhl.sms.caster.utils.handler.error

import com.vhl.sms.caster.Logger
import com.vhl.sms.caster.commons.errors.BasicErrors
import com.vhl.sms.caster.commons.response.Response

inline fun <T> responseCatch(body: () -> Response<T>): Response<T> {
   return try {
      body()
   } catch (e: Exception) {
      return when (e) {
         is BasicErrors -> {
            Response(e.errorCode, e.message, null)
         }
         else -> {
            Logger.error { e.message }
            e.printStackTrace()
            Response("500", e.message ?: "Unknown", null)
         }
      }
   }
}

