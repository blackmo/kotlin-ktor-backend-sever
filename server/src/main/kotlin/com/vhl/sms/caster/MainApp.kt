package com.vhl.sms.caster

import com.vhl.sms.caster.authentication.JwtConfig
import com.vhl.sms.caster.constants.ModuleTags
import com.vhl.sms.caster.controller.web.UserController
import com.vhl.sms.caster.controller.web.school.PersonController
import com.vhl.sms.caster.database.DBSource
import com.vhl.sms.caster.di.LoadedModules
import com.vhl.sms.caster.features.TokenValidation
import com.vhl.sms.caster.router.SocketRouterX
import com.vhl.sms.caster.service.TokenSessionService
import com.vhl.sms.caster.service.UserPrincipalService
import com.vhl.sms.caster.storage.redis.Redis
import com.vhl.sms.caster.storage.session.RedisSessionStorage
import com.vhl.sms.caster.utils.formatter.LocalDateTypeAdapter
import getProperty
import getPropertyList
import installCorsFromConfig
import io.ktor.application.Application
import io.ktor.application.ApplicationCallPipeline
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.auth.Authentication
import io.ktor.auth.jwt.jwt
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.gson.gson
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.http.cio.websocket.pingPeriod
import io.ktor.http.cio.websocket.timeout
import io.ktor.request.path
import io.ktor.routing.routing
import io.ktor.sessions.*
import io.ktor.util.KtorExperimentalAPI
import io.ktor.util.generateNonce
import io.ktor.websocket.WebSockets
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import localHost
import mu.KotlinLogging
import org.kodein.di.Kodein
import org.kodein.di.generic.instance
import org.slf4j.event.Level
import java.time.Duration
import java.time.LocalDate


val Logger = KotlinLogging.logger { }

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)


object KodeinInstance {
    var kodein: Kodein? = null
}

@ExperimentalCoroutinesApi
@KtorExperimentalAPI
fun Application.messageModule() = runBlocking {

    KodeinInstance.kodein = LoadedModules(environment).kodein
    val kodein = KodeinInstance.kodein ?: throw IllegalStateException("Kodein DI not intialized")

    val dbSource = DBSource(environment)
    dbSource.initDataBaseConnection(DBSource.MYSQL_DB)
    dbSource.createSchema()

    val webSocket: SocketRouterX by kodein.instance<SocketRouterX>()

    //-- controllers
    val userController: UserController by kodein.instance<UserController>()
    val personController: PersonController by kodein.instance<PersonController>()
    // authentication
    val jwtConfig: JwtConfig by kodein.instance<JwtConfig>()
    val userPrincipalService: UserPrincipalService by kodein.instance<UserPrincipalService>()

    //-- services
    val redisClient: Redis? by kodein.instance<Redis>(ModuleTags.REDIS_DB1)
    val tokenService: TokenSessionService by kodein.instance<TokenSessionService>()

//    redisToken.async.flushdb()
//    Logger.debug { "Server :: token db flushed" }

    val expiration = environment.getProperty("redis.expiration").toInt()

    install(ContentNegotiation) {
        gson {
            registerTypeAdapter(LocalDate::class.java, LocalDateTypeAdapter().nullSafe())
            setPrettyPrinting()
        }
    }

    install(CallLogging) {
        level = Level.INFO
        filter { call -> call.request.path().startsWith("/home") }
    }

    install(Authentication) {
        jwt {
            verifier(jwtConfig.verifier)
            realm = "ktor.io"
            validate { principal ->
                principal.payload.getClaim("id").asInt().let { userPrincipalService.findOneById(it) }
            }
        }
    }

    install(WebSockets) {
        pingPeriod      = Duration.ofSeconds(100)
        timeout         = Duration.ofSeconds(100)
        maxFrameSize    = Long.MAX_VALUE
    }

    install(Sessions) {
        redisClient?.run {
            cookie<SocketRouterX.ChatSession>("SESSION", storage = RedisSessionStorage(this, expiration.toLong())) {
                cookie.path = "/"
            }
        }
    }

    installCorsFromConfig{
        val localPorts = environment
            .getPropertyList("cors.local.ports")
            .map { it.toInt() }

        method(HttpMethod.Get)
        method(HttpMethod.Post)
        header(HttpHeaders.AccessControlAllowOrigin)
        localHost(*localPorts.toIntArray())
    }

    //-- custom feature
    install(TokenValidation) {
        tokenSessionService = tokenService
    }

    routing {
        intercept(ApplicationCallPipeline.Features) {
            if (call.sessions.get<SocketRouterX.ChatSession>() == null) {
                call.sessions.set(SocketRouterX.ChatSession(generateNonce(), System.currentTimeMillis() + expiration))
            }
        }
    }

    routing {
        userController.createRoute("home", this)
        webSocket.createWebSocket("home", this)
        personController.createRoute("home", this)
    }
}

