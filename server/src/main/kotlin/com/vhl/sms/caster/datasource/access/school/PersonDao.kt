package com.vhl.sms.caster.datasource.access.school

import com.vhl.sms.caster.commons.model.school.Person
import com.vhl.sms.caster.datasource.access.UserDoa
import com.vhl.sms.caster.exposed.Dao
import com.vhl.sms.caster.exposed.Domain
import org.jetbrains.exposed.sql.Alias
import org.jetbrains.exposed.sql.Expression
import org.jetbrains.exposed.sql.`java-time`.date
import java.time.LocalDate
import java.time.LocalDateTime

object PersonDao: Dao("person") {
    val firstName = varchar("first_name", 50)
    val middleName = varchar("middle_name", 50).nullable()
    val lastName = varchar("last_name", 50)
    val address = varchar("address", 250)
    val gender = varchar("gender", 15).nullable()
    val mobileNumber = varchar("mobile_number", 20).nullable()
    val birthday = date("birth_date").nullable()

    val createdBy = integer("created_by").references(UserDoa.id).nullable()
    val modifiedBy = integer("modified_by").references(UserDoa.id).nullable()

}

class PersonAccess: Domain {

    override var id: Int = 0
    var firstName = ""
    var middleName: String? = ""
    var lastName = ""
    var gender: String? = ""
    var address = ""
    var mobileNo: String? = ""
    var birthday: LocalDate? = null

    var createdBy: Int? = null
    var modifiedBy: Int? = null

    override var createdAt: LocalDateTime? = null
    override var modifiedAt: LocalDateTime? = null

    fun toPerson() = Person(
        id,
        firstName,
        middleName,
        lastName,
        gender,
        address,
        mobileNo,
        birthday
    )


    infix fun from(person: Person?): PersonAccess {
        person?.let {
            id = person.id
            firstName = person.firstName
            middleName  = person.middleName
            lastName = person.lastName
            gender = person.gender
            address = person.address
            mobileNo = person.mobileNo
            birthday = person.birthday
        }

        return this
    }

    infix fun itsId(id: Int): PersonAccess {
        this.id = id
        return this
    }
}

fun personFields() = listOf<Expression<*>>(
    PersonDao.firstName,
    PersonDao.middleName,
    PersonDao.lastName,
    PersonDao.gender,
    PersonDao.address,
    PersonDao.mobileNumber,
    PersonDao.birthday,
    PersonDao.id
)


fun personFieldWithAlias(table: Alias<PersonDao>) = listOf<Expression<*>>(
    table[PersonDao.createdAt],
    table[PersonDao.modifiedAt],
    table[PersonDao.firstName],
    table[PersonDao.middleName],
    table[PersonDao.lastName],
    table[PersonDao.gender],
    table[PersonDao.address],
    table[PersonDao.mobileNumber],
    table[PersonDao.birthday],
    table[PersonDao.id]
)

fun personSoftFieldsWithAlias(table: Alias<PersonDao>) = listOf<Expression<*>>(
    table[PersonDao.firstName],
    table[PersonDao.middleName],
    table[PersonDao.lastName],
    table[PersonDao.gender],
    table[PersonDao.address],
    table[PersonDao.mobileNumber],
    table[PersonDao.birthday]
)
