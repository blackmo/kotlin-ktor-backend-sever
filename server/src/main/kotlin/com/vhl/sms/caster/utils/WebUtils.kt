import com.beust.klaxon.Klaxon
import com.vhl.sms.caster.Logger
import com.vhl.sms.caster.commons.errors.E_NO_SESSION
import com.vhl.sms.caster.commons.errors.E_SESSION_EXPIRED
import com.vhl.sms.caster.commons.response.Response
import com.vhl.sms.caster.model.SimpleTokenModel
import com.vhl.sms.caster.router.SocketRouterX
import io.ktor.application.Application
import io.ktor.application.ApplicationCall
import io.ktor.application.ApplicationEnvironment
import io.ktor.application.install
import io.ktor.features.CORS
import io.ktor.http.HttpStatusCode
import io.ktor.request.ApplicationRequest
import io.ktor.response.respond
import io.ktor.sessions.clear
import io.ktor.sessions.get
import io.ktor.sessions.sessions
import io.ktor.util.KtorExperimentalAPI
import org.apache.commons.codec.binary.Base64

@KtorExperimentalAPI
fun ApplicationEnvironment.getProperty(keyDir: String) =
    this.config.property(keyDir).getString()

@KtorExperimentalAPI
fun ApplicationEnvironment.getPropertyList(keyDir: String) =
    this.config.property(keyDir).getList()

suspend fun ApplicationCall.validateSession(): Boolean {
    val session = sessions.get<SocketRouterX.ChatSession>()

    return when {
        null == session -> {
            response.status(HttpStatusCode.BadRequest)
            respond(Response<Any>(E_NO_SESSION, "Request Denied"))
            false
        }
        System.currentTimeMillis() > session.expiration -> {
            Logger.debug {"session: expired"}
            response.status(HttpStatusCode.Unauthorized)
            respond(Response<Any>(E_SESSION_EXPIRED, "Session Expired"))
            sessions.clear<SocketRouterX.ChatSession>()
            false
        }
        else -> true
    }
}

suspend  inline fun ApplicationCall.validateDeviceId(action: () -> Unit) {
    val sessionId = request.headers["deviceSession"]
    if (sessionId == null) {
        response.status(HttpStatusCode.BadRequest)
        respond(Response<Any>("E_NO_DEVICE_ID", "Invalid request"))
    } else {
        action()
    }
}

fun extractTokenModel(token: String): SimpleTokenModel? {
    val partials = token.split(".")
    val content = String(Base64.decodeBase64(partials[1]))
    return Klaxon().parse(content)

}

suspend fun ApplicationRequest.getTokenModel() =
    extractTokenModel(headers["Authorization"]!!.removePrefix("Bearer"))

suspend inline fun Application.installCorsFromConfig(crossinline body: CORS.Configuration.() -> Unit) {
    try {
        val enable = environment.getProperty("cors.enabled").toBoolean()
        if (enable) {
            install(CORS) { body() }
        }
    } catch (e: Exception) {
        Logger.error { "Server:: Error Occurred - ${e.message}" }
    }
}

fun CORS.Configuration.localHost(vararg ports: Int) {
      ports.forEach { port ->
          host("localhost:$port")
      }
}
