package com.vhl.sms.caster.authentication

import io.ktor.auth.Principal

data class UserPrincipal(
    val id: Int,
    val userName: String
): Principal