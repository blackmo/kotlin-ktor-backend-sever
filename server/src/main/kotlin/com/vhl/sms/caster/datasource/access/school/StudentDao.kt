package com.vhl.sms.caster.datasource.access.school

import com.vhl.sms.caster.commons.model.school.Student
import com.vhl.sms.caster.datasource.access.UserDoa
import com.vhl.sms.caster.exposed.Dao
import com.vhl.sms.caster.exposed.Domain
import java.time.LocalDateTime

object StudentDao: Dao("student") {
    val lrnID = varchar("lrn_id", 12).uniqueIndex()
    val person = (integer("person_id") references PersonDao.id).uniqueIndex()
    val mother = (integer("mother_person_id") references PersonDao.id).nullable()
    val father = (integer("father_person_id") references PersonDao.id).nullable()
    val status = varchar("status", 30).default("NEW")

    val createdBy = integer("created_by").references(UserDoa.id).nullable()
    val modifiedBy = integer("modified_by").references(UserDoa.id).nullable()
}

class StudentAccess: Domain {

    override var id: Int = 0
    var lrnID: String = ""
    var person: PersonAccess? = null
    var mother: PersonAccess? = null
    var father: PersonAccess? = null
    var status: String? = ""
    var createdBy: Int? = null
    var modifiedBy: Int? = null

    override var createdAt: LocalDateTime? = null
    override var modifiedAt: LocalDateTime? = null

    infix fun from(student: Student): StudentAccess {
        id = student.id
        lrnID = student.lrnID
        person = PersonAccess() from student.person
        mother = student.mother?.let { PersonAccess() from it }
        father = student.father?.let { PersonAccess() from it }

        return this
    }

    infix fun itsID(id: Int): StudentAccess {
        this.id = id
        return this
    }

    fun toStudent(): Student {
        return Student(
            id,
            lrnID,
            person?.toPerson() ?: throw IllegalArgumentException("Empty student person"),
            mother?.toPerson(),
            father?.toPerson()
        )
    }
}
