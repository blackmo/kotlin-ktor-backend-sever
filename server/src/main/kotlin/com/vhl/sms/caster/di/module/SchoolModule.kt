package com.vhl.sms.caster.di.module

import com.vhl.sms.caster.controller.web.school.*
import com.vhl.sms.caster.repository.school.AdviserRepository
import com.vhl.sms.caster.repository.school.AdvisoryRepository
import com.vhl.sms.caster.repository.school.PersonRepository
import com.vhl.sms.caster.repository.school.StudentRepository
import com.vhl.sms.caster.repository.school.advisory.RefAdvisoryStudentRepository
import com.vhl.sms.caster.service.school.AdviserService
import com.vhl.sms.caster.service.school.AdvisoryService
import com.vhl.sms.caster.service.school.PersonService
import com.vhl.sms.caster.service.school.StudentService
import com.vhl.sms.caster.service.school.advisory.RefAdvisoryStudentService
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider

class SchoolModule {
    companion object {
        fun personRouteModule () = Kodein.Module("person-module") {
            bind() from provider { PersonRepository() }
            bind() from provider { PersonService(instance()) }
            bind() from provider { PersonController(instance()) }
        }

        fun studentRouteModule() = Kodein.Module("student-module") {
            bind() from provider { StudentRepository(instance()) }
            bind() from provider { StudentService(instance(), instance()) }
            bind() from provider { StudentController(instance()) }
        }

        fun adviserRouteModule() = Kodein.Module("adviser-module") {
            bind() from provider { AdviserRepository(instance()) }
            bind() from provider { AdviserService(instance(), instance()) }
            bind() from provider { AdviserController(instance()) }
        }

        fun advisoryRouteModule() = Kodein.Module("advisory-module") {
            bind() from provider { AdvisoryRepository(instance(), instance()) }
            bind() from provider { AdvisoryService(instance(), instance()) }
            bind() from provider { AdvisoryController(instance()) }
        }

        fun refAdvisoryModule() = Kodein.Module("ref-advisory-module") {
            bind() from provider { RefAdvisoryStudentRepository(instance()) }
            bind() from provider { RefAdvisoryStudentService(instance(), instance(), instance()) }
            bind() from provider { RefAdvisoryStudentController(instance()) }
        }
    }
}