package com.vhl.sms.caster.controller.web.school

import com.vhl.sms.caster.commons.errors.InvalidRequiredInputsError
import com.vhl.sms.caster.commons.errors.MissingRequiredInputsError
import com.vhl.sms.caster.commons.model.school.Student
import com.vhl.sms.caster.commons.model.school.advisory.RefAdvisoryStudent
import com.vhl.sms.caster.commons.payload.outgoing.StudentJoinSoftPrsn
import com.vhl.sms.caster.commons.payload.school.student.SearchStudentP
import com.vhl.sms.caster.commons.response.Response
import com.vhl.sms.caster.features.validateToken
import com.vhl.sms.caster.service.school.advisory.RefAdvisoryStudentService
import com.vhl.sms.caster.utils.StatusResponse
import getTokenModel
import io.ktor.application.call
import io.ktor.auth.authenticate
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.*

class RefAdvisoryStudentController(
    private val refAdvisoryService: RefAdvisoryStudentService
) {
    fun createRoute(root: String, routing: Routing) {
        routing {
            val base = "$root/school/ref-advisory"
            validateToken {
                authenticate {
                    addReference("$base/add")
                    deleteStudent("$base/delete")
                    findStudentsByCriteria("$base/find/by")
                    getStudentsFromAdvisory("$base/students/{id}")
                }
            }
        }
    }

    private fun Route.addReference(url: String) {
        post(url) {
            val dataResponse: Response<String?>
            val reference = call.receive<RefAdvisoryStudent>()
            val tokenModel = call.request.getTokenModel()!!

            dataResponse = refAdvisoryService.addReference(reference, tokenModel.id) { code, message, dataValue ->
                Response(code, message, dataValue)
            }

            if (dataResponse.code != StatusResponse.SUCCESS) {
                call.response.status(HttpStatusCode.BadRequest)
            }
            call.respond(dataResponse)
        }
    }

    private fun Route.getStudentsFromAdvisory(url: String) {
        get(url) {
            try {
                val dataResponse: Response<List<StudentJoinSoftPrsn>>
                val id = call.parameters["id"] ?: throw MissingRequiredInputsError("Missing Id", "REF_ADVISORY_SC")

                dataResponse = refAdvisoryService.findStudentsOnAdvisory(id.toInt()) { code, message, dataValue ->
                    Response(code, message, dataValue)
                }

                if (dataResponse.code != StatusResponse.SUCCESS) {
                    call.response.status(HttpStatusCode.BadRequest)
                }
                call.respond(dataResponse)

            } catch (e: NumberFormatException) {
                val error = InvalidRequiredInputsError("Invalid ID Format", "ADVISORY_SC")
                call.response.status(HttpStatusCode.BadRequest)
                call.respond(Response<String>(error.errorCode, error.message, null))
            }
        }
    }

    private fun Route.deleteStudent(url: String) {
        delete(url) {
            val dataResponse: Response<String?>
            val reference = call.receive<RefAdvisoryStudent>()

            dataResponse = refAdvisoryService.deleteReference(reference) { code, message, dataValue ->
                Response(code, message, dataValue)
            }

            if (dataResponse.code != StatusResponse.SUCCESS) {
                call.response.status(HttpStatusCode.BadRequest)
            }

            call.respond(dataResponse)
        }
    }

    private fun Route.findStudentsByCriteria(url: String) {
        post(url) {
            val dataResponse: Response<List<Student>?>
            val reference = call.receive<SearchStudentP>()

            dataResponse = refAdvisoryService.findStudentByCriteria(reference) { code, message, dataValue ->
                Response(code, message, dataValue)
            }

            if (dataResponse.code != StatusResponse.SUCCESS) {
                call.response.status(HttpStatusCode.BadRequest)
            }

            call.respond(dataResponse)
        }
    }
}