package com.vhl.sms.caster.di.module

import com.vhl.sms.caster.authentication.JwtConfig
import com.vhl.sms.caster.service.UserPrincipalService
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

class AuthModule {
    companion object {
        fun authentication(secret: String, expiration: Long) = Kodein.Module("authentication-module") {
            bind() from singleton { JwtConfig(secret, expiration) }
            bind() from singleton { UserPrincipalService(instance()) }
        }
    }
}