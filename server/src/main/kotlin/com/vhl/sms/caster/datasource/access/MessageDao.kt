package com.vhl.sms.caster.datasource.access

import com.vhl.sms.caster.commons.model.Message
import com.vhl.sms.caster.exposed.Dao
import com.vhl.sms.caster.exposed.Domain
import org.jetbrains.exposed.sql.`java-time`.date
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

object MessageDao : Dao("message") {
    val body        = varchar("content", 255)
    val date        = date("date").nullable()
    val status      = varchar("status", 20)
    val contact     = (integer("person_id") references(ContactDao.id)).nullable()
}

class MessageAccess: Domain {
    override var id               : Int = 0
    var body            : String  = ""
    var date            : LocalDate? = null
    var status          : String = ""
    var contact         : ContactAccess ? = null

    override var createdAt  : LocalDateTime? = null
    override var modifiedAt : LocalDateTime? = null

    infix fun from (message: Message): MessageAccess {

        val dateFormatter = DateTimeFormatter.ofPattern("MM-dd-yyyy HH:mi")

        id = message.id
        body = message.body
        date = message.date.let {  LocalDate.parse(it, dateFormatter)}
        status = message.status
        contact = ContactAccess() from message.contact
        return this
    }
}

