package com.vhl.sms.caster.repository.school

import com.vhl.sms.caster.commons.errors.AlreadyExistError
import com.vhl.sms.caster.datasource.access.school.*
import com.vhl.sms.caster.exposed.ExposedSchema
import com.vhl.sms.caster.repository.DataRepository
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.UpdateBuilder
import org.jetbrains.exposed.sql.transactions.transaction

class AdviserRepository(private val personRepository: PersonRepository):
    ExposedSchema<AdviserAccess, AdviserDao>(), DataRepository<AdviserAccess> {

    override val entityName: String = "Adviser"
    override val originName: String = "ORIGIN_ADVISER"

    private val fieldSets = mutableListOf<Expression<*>>()
        .also {
            it.add(AdviserDao.id)
            it.add(AdviserDao.licenseNumber)
            it.add(AdviserDao.createdAt)
            it.add(AdviserDao.modifiedAt)
            it.addAll(personFields())
        }

    override fun add(entity: AdviserAccess): AdviserAccess = transaction {
        AdviserDao.checkIfAlreadyExist {
            AdviserDao.licenseNumber eq entity.licenseNumber
        }.run {
            if (this) throw AlreadyExistError(
                "Adviser already exist with license number : ${entity.licenseNumber}",
                originName
            )
        }

        AdviserDao.insertOne(entity)
    }

    override fun delete(entity: AdviserAccess) = transaction {
        AdviserDao.deleteWhere { AdviserDao.id eq entity.id }
        commit()
    }

override fun update(entity: AdviserAccess): AdviserAccess = transaction {
    AdviserDao.updateOne(entity) {
        AdviserDao.id eq entity.id
    }
}

    override fun findOneById(id: Int): AdviserAccess? = transaction {
        AdviserDao.select {
            AdviserDao.id eq id
        }.mapNotNull { it.toEntity() }.firstOrNull()
    }

    fun findOneEager(id: Int): AdviserAccess? = transaction {
        findByEagerAction { AdviserDao.id eq  id }
            .mapNotNull { it.joinWithPerson() }.firstOrNull()
    }

    fun findByEagerLike(
        lastName: String,
        firstName: String?,
        middleName: String?,
        limit: Boolean,
        count: Int = 0,
        offset: Int = 0
    ) = transaction {
        var query = findByEagerAction { PersonDao.lastName like "$lastName%" }

        query = firstName?.let {
            query.andWhere { PersonDao.firstName like "$it%" }
        } ?: query

        query = middleName?.let {
            query.andWhere { PersonDao.middleName like "$it%" }
        } ?: query

        query.apply {
            if (limit) {
                query.limit(count, offset.toLong())
            }
        }.mapNotNull { it.joinWithPerson() }
    }

    fun findAllByEager() = transaction {
        AdviserDao
            .innerJoin(PersonDao)
            .slice(fieldSets)
            .selectAll()
            .mapNotNull { it.joinWithPerson() }
    }

    override fun findAllAndPaginate(count: Int, offset: Int): List<AdviserAccess>  = transaction{
        AdviserDao.findAllAndPaginate(count, offset)
    }

    override fun UpdateBuilder<Any>.from(entity: AdviserAccess, dao: AdviserDao) {
        this[dao.id] = entity.id
        this[dao.licenseNumber] = entity.licenseNumber
        this[dao.createdBy] = entity.createdBy
        this[dao.modifiedBy] = entity.modifiedBy

        entity.person?.id?.let { this[dao.person] = it }

    }

    override fun ResultRow.toEntity(): AdviserAccess {
        val adviserAccess = AdviserAccess()

        adviserAccess.id = this[AdviserDao.id]
        adviserAccess.licenseNumber = this[AdviserDao.licenseNumber]
        adviserAccess.person = PersonAccess() itsId this[AdviserDao.person]

        adviserAccess.createdAt = this[AdviserDao.createdAt]
        adviserAccess.modifiedAt = this[AdviserDao.modifiedAt]

        return adviserAccess
    }

    private fun findByEagerAction(where: SqlExpressionBuilder.() -> Op<Boolean>) =
        AdviserDao
            .innerJoin(PersonDao)
            .slice(fieldSets)
            .select(where)

    fun ResultRow.joinWithPerson(): AdviserAccess {
        val adviserAccess = AdviserAccess()
        val person = personRepository.fromResultRow(this)

        adviserAccess.id = this[AdviserDao.id]
        adviserAccess.licenseNumber = this[AdviserDao.licenseNumber]
        adviserAccess.person = person

        return adviserAccess
    }

    fun fromResultRow(resultRow: ResultRow): AdviserAccess {
        return resultRow.toEntity()
    }

    fun joinAdviserWithPerson(resultRow: ResultRow): AdviserAccess {
       return resultRow.joinWithPerson()
    }
}