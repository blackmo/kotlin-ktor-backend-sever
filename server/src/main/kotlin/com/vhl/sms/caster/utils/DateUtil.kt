package com.vhl.sms.caster.utils

import java.text.SimpleDateFormat
import java.util.*

class DateUtil {
    companion object {
        fun createDate(year: Int, month: Int, day: Int): Date {
            return SimpleDateFormat("MM/dd/yyyy").parse("$month/${day+1}/$year")
        }
    }
}