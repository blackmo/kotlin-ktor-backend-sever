package com.vhl.sms.caster.commons.payload.incomming

import com.vhl.sms.caster.commons.model.MessageReceived

data class AllMessages (
    val receivedMessages: List<MessageReceived>
)
