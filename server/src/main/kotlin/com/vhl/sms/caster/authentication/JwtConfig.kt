package com.vhl.sms.caster.authentication

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm
import com.vhl.sms.caster.commons.model.User
import java.util.*

class JwtConfig(
    private val secret: String,
    val validityInMiliSeconds: Long = 3_600_000 * 5,// 5 hour/s
    private val issuer: String = "smscaster.vhl"
) {

    private val algorithm = Algorithm.HMAC256(secret)

    val verifier: JWTVerifier = JWT
        .require(algorithm)
        .withIssuer(issuer)
        .build()

    fun createToken(user: User): String = JWT.create()
        .withSubject("Authentication")
        .withIssuer(issuer)
        .withClaim("id", user.id)
        .withExpiresAt(getExpirationAt())
        .sign(algorithm)

    fun getExpirationAt() = Date(System.currentTimeMillis() + validityInMiliSeconds)

}