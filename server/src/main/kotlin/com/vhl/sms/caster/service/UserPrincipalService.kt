package com.vhl.sms.caster.service

import com.vhl.sms.caster.authentication.UserPrincipal
import com.vhl.sms.caster.repository.UserRepository

class UserPrincipalService(
    private val userRepository: UserRepository
) {
    fun findOneById(id: Int): UserPrincipal? {
        return userRepository.findOneById(id)?.let {
            UserPrincipal(it.id, it.userName)
        }
    }
}