package com.vhl.sms.caster.service.upload

import com.vhl.sms.caster.commons.model.school.Person
import com.vhl.sms.caster.commons.model.school.Student
import com.vhl.sms.caster.commons.payload.school.student.batch.BRErrorStudentResponse
import com.vhl.sms.caster.commons.payload.school.student.batch.BRegistrationResponse
import com.vhl.sms.caster.commons.response.Response
import com.vhl.sms.caster.service.school.StudentService
import com.vhl.sms.caster.utils.StatusResponse
import com.vhl.sms.caster.utils.file.validateReadCSVTemplate
import java.io.File
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class UploadService(
    private val studentService: StudentService
) {
    fun receiveStudentBatchUpload(file: File, userId: Int): BRegistrationResponse? {
        val path = "/template/csv/student-upload/register-template.csv"
        val resourceStream = this::class.java.getResourceAsStream(path)
        val listStudent = mutableListOf<Student>()
        val listFailed = mutableListOf<BRErrorStudentResponse>()

        return validateReadCSVTemplate(resourceStream, file) {
            readAllAsSequence().forEachIndexed { index, list ->
                val student = processContent(list)
                val response = studentService.add(student, userId) { code, message, dataValue ->
                    Response(code, message, dataValue)
                }
                when (response.code) {
                    StatusResponse.SUCCESS -> {
                        response.data?.run {
                            listStudent.add(this)
                        }
                    }
                    else -> {
                        listFailed.add(
                            BRErrorStudentResponse(
                                response.message,
                                student
                            )
                        )
                    }
                }
            }
            BRegistrationResponse(
                listStudent,
                listFailed
            )
        }
    }
}

private fun processContent(list: List<String>): Student {
    val lrnId = list[0]
    val studentPerson = list.subList(1, 8).toPerson()!!
    val father = list.subList(8, 15).toPerson()
    val mother = list.subList(15, 22).toPerson()

    return Student(
        0,
        lrnId,
        studentPerson,
        mother,
        father
    )
}

private fun List<String>.toPerson(): Person? {
    val firstName = this[0]
    val middleName = this[1].let {
        if (it.isBlank()) null
        else it
    }

    val lastName = this[2]

    val birthDay = this[3].let {
        return@let when {
            !it.isBlank() -> {
                LocalDate.parse(it, DateTimeFormatter.ofPattern("MM/dd/yyyy"))
            }
            else -> null
        }
    }
    val gender = this[4].capitalize()
    val address = this[5]
    val mobileNo = this[6]

    if (firstName.isBlank() || lastName.isBlank()) return null

    return Person(
        0,
        firstName,
        middleName,
        lastName,
        gender,
        address,
        mobileNo,
        birthDay
    )
}
