package com.vhl.sms.caster.repository.school.advisory

import com.vhl.sms.caster.commons.constants.school.student.status.StudentStatus
import com.vhl.sms.caster.commons.errors.AlreadyExistError
import com.vhl.sms.caster.commons.errors.InvalidRequiredInputsError
import com.vhl.sms.caster.commons.payload.school.student.SearchStudentP
import com.vhl.sms.caster.datasource.access.school.*
import com.vhl.sms.caster.datasource.access.school.advisory.RefAdvisoryStudentAccess
import com.vhl.sms.caster.datasource.access.school.advisory.RefAdvisoryStudentDao
import com.vhl.sms.caster.exposed.ExposedSchema
import com.vhl.sms.caster.repository.school.StudentRepository
import com.vhl.sms.caster.utils.formatter.notEmptyLike
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.UpdateBuilder
import org.jetbrains.exposed.sql.transactions.transaction

class RefAdvisoryStudentRepository(
    private val studentRepository: StudentRepository

) : ExposedSchema<RefAdvisoryStudentAccess, RefAdvisoryStudentDao>() {
    override val entityName = "Advisory_Student"
    override val originName = "ORIGIN_ADVISORY_STUDENT"

    companion object {
        private const val ALIAS_PERSON = "person"
        private const val ALIAS_MOTHER = "mother"
        private const val ALIAS_FATHER = "father"
    }

    private val personTable = PersonDao.alias(ALIAS_PERSON)
    private val motherTable = PersonDao.alias(ALIAS_MOTHER)
    private val fatherTable = PersonDao.alias(ALIAS_FATHER)

    private val fieldSets = mutableListOf<Expression<*>>()
        .also {
            it.add(StudentDao.id)
            it.add(StudentDao.lrnID)
            it.add(StudentDao.createdAt)
            it.add(StudentDao.modifiedAt)
            it.add(StudentDao.status)
            it.addAll(personFieldWithAlias(personTable))
            it.addAll(personFieldWithAlias(motherTable))
            it.addAll(personFieldWithAlias(fatherTable))
        }

    private val studentFieldSets = mutableListOf<Expression<*>>()
        .also {
            it.add(StudentDao.lrnID)
            it.add(StudentDao.id)
            it.add(StudentDao.status)
            it.addAll(personFields())
        }

    fun addReference(entity: RefAdvisoryStudentAccess) = transaction {
        RefAdvisoryStudentDao.checkIfAlreadyExist {
            (RefAdvisoryStudentDao.student eq hasId("Student", entity.student?.id)) and
                    (RefAdvisoryStudentDao.advisory eq hasId("Advisory", entity.advisory?.id))
        }.run { if (this) throw AlreadyExistError("Student is already Referenced to class Advisory", originName) }
        RefAdvisoryStudentDao.insertOne(entity)

        studentRepository.updateStudentStatus(entity.student?.id!!, StudentStatus.ENROLLED)
    }

    fun deleteReference(entity: RefAdvisoryStudentAccess) = transaction {
        RefAdvisoryStudentDao.deleteWhere {
            (RefAdvisoryStudentDao.student eq hasId("Student", entity.student?.id)) and
                    (RefAdvisoryStudentDao.advisory eq hasId("Student", entity.advisory?.id))
        }
        commit()
    }

    fun findStudentOnAdvisory(advisoryId: Int) = transaction {
        RefAdvisoryStudentDao
            .innerJoin(StudentDao)
            .innerJoin(PersonDao, { PersonDao.id }, { StudentDao.person })
            .slice(studentFieldSets)
            .select { RefAdvisoryStudentDao.advisory eq advisoryId }
            .mapNotNull { studentRepository.studentSoftPersonFields(it) }
    }

    fun findStudentsByCriteria(criteria: SearchStudentP) = transaction {
        var queryOp = RefAdvisoryStudentDao
            .innerJoin(StudentDao, { student }, { StudentDao.id })
            .innerJoin(AdvisoryDao, { RefAdvisoryStudentDao.advisory }, { AdvisoryDao.id })
            .leftJoin(personTable, { StudentDao.person }, { personTable[PersonDao.id] })
            .leftJoin(motherTable, { StudentDao.mother }, { motherTable[PersonDao.id] })
            .leftJoin(fatherTable, { StudentDao.father }, { fatherTable[PersonDao.id] })
            .slice(fieldSets)
            .selectAll()

        //-- filter bv last name
        queryOp = when (criteria.lastName) {
            null -> queryOp
            "" -> queryOp
            else -> queryOp.andWhere { PersonDao.lastName like criteria.lastName!!.notEmptyLike() }
        }

        //-- filter by classYear
        queryOp = when (criteria.classYear) {
            0, null -> queryOp
            else -> queryOp.andWhere { AdvisoryDao.classYear eq criteria.classYear!! }
        }

        //-- filter bv class level
        queryOp = when (criteria.classLevel?.toUpperCase()) {
            null, "", "ALL" -> queryOp
            else -> queryOp.andWhere { AdvisoryDao.classLevel eq criteria.classLevel!!.toInt() }
        }

        //-- filter bv class section
        queryOp = when (criteria.classSection?.toUpperCase()) {
            null, "", "ALL" -> queryOp
            else -> queryOp.andWhere { AdvisoryDao.sectionNumber eq criteria.classSection!!.toInt() }
        }

        queryOp.mapNotNull { studentRepository.joinEagerStudentWithParents(it) }
    }

    override fun UpdateBuilder<Any>.from(entity: RefAdvisoryStudentAccess, dao: RefAdvisoryStudentDao) {
        this[dao.id] = entity.id
        this[dao.student] = hasId("Student", entity.student?.id)
        this[dao.advisory] = hasId("Advisory", entity.advisory?.id)

        entity.createdBy?.let { this[dao.createdBy] = it }
        entity.modifiedBy?.let { this[dao.modifiedBy] = it }
    }

    override fun ResultRow.toEntity(): RefAdvisoryStudentAccess {
        val advisoryAccess = RefAdvisoryStudentAccess()
        advisoryAccess.id = this[RefAdvisoryStudentDao.id]
        advisoryAccess.advisory = AdvisoryAccess() itsID this[RefAdvisoryStudentDao.advisory]
        advisoryAccess.student = StudentAccess() itsID this[RefAdvisoryStudentDao.student]

        advisoryAccess.createdAt = this[RefAdvisoryStudentDao.createdAt]
        advisoryAccess.modifiedAt = this[RefAdvisoryStudentDao.modifiedAt]

        return advisoryAccess
    }

    private fun hasId(name: String, id: Int?): Int {
        when (id) {
            null, 0 -> {
                throw InvalidRequiredInputsError("$name not properly initialized", originName)
            }
            else -> return id
        }
    }
}