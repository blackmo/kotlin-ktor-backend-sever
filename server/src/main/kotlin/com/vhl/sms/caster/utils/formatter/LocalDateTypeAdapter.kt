package com.vhl.sms.caster.utils.formatter

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class LocalDateTypeAdapter : TypeAdapter<LocalDate>() {
    private val formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy")
    override fun write(out: JsonWriter?, value: LocalDate?) {
        out?.value(formatter.format((value)))
    }

    override fun read(input: JsonReader?): LocalDate {
        return LocalDate.parse(input?.nextString(), formatter)
    }
}