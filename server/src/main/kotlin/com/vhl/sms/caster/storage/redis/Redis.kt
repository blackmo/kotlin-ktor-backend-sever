package com.vhl.sms.caster.storage.redis

import com.vhl.sms.caster.Logger
import io.lettuce.core.RedisClient
import io.lettuce.core.RedisConnectionException
import io.lettuce.core.RedisURI
import io.lettuce.core.api.StatefulRedisConnection
import io.lettuce.core.api.async.RedisAsyncCommands

class Redis private  constructor(){
    lateinit var redisClient    : RedisClient
    lateinit var connection     : StatefulRedisConnection<String, String>
    lateinit var async          : RedisAsyncCommands<String, String>

    companion object {
        @Throws(RedisConnectionException::class)
        fun createClient(host: String, port: Int, db: Int, password: String?): Redis {
            val instance = Redis()
            try {
                instance.createConnection(host, port, db, password)
            } catch (e: Exception) {
                e.printStackTrace()
                instance.connection.close()
            }
            return instance
        }

        @Throws(RedisConnectionException::class)
        fun createClient(host: String, port: Int, db: Int) =
            Redis().createConnection(host, port, db, null)
    }

    private fun createConnection(host: String, port: Int, db: Int, password: String?): Redis {
        val client = RedisURI.Builder
            .redis(host, port)
            .withDatabase(db)
            .also {
                if (!password.isNullOrBlank()) {
                    it.withPassword(password)
                }
            }.build()
        redisClient     = RedisClient.create(client)
        connection      = redisClient.connect()
        async           = connection.async()

        Logger.debug { "Success fully connected to redis server ::  ${client.host}:${client.port}" }

        return this
    }
}