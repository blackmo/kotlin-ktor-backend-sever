package com.vhl.sms.caster.repository.school

import com.vhl.sms.caster.commons.errors.AlreadyExistError
import com.vhl.sms.caster.commons.errors.InvalidRequiredInputsError
import com.vhl.sms.caster.commons.payload.outgoing.AdvisoryJoinAdviserPrsn
import com.vhl.sms.caster.commons.payload.school.adviory.SearchAdvisoryP
import com.vhl.sms.caster.datasource.access.school.*
import com.vhl.sms.caster.exposed.ExposedSchema
import com.vhl.sms.caster.repository.DataRepository
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.UpdateBuilder
import org.jetbrains.exposed.sql.transactions.transaction

class AdvisoryRepository(
    private val adviserRepository: AdviserRepository,
    private val personRepository: PersonRepository
) : ExposedSchema<AdvisoryAccess, AdvisoryDao>(), DataRepository<AdvisoryAccess> {

    override val entityName: String = "Advisory"
    override val originName: String = "ORIGIN_ADVISORY"

    private val fieldSets = mutableListOf<Expression<*>>()
        .also {
            it.add(AdvisoryDao.id)
            it.add(AdvisoryDao.classYear)
            it.add(AdvisoryDao.classLevel)
            it.add(AdvisoryDao.advisoryName)
            it.add(AdvisoryDao.sectionNumber)
            it.add(AdvisoryDao.sectionName)
            it.add(AdvisoryDao.sectionName)
            it.addAll(personFields())
        }

    override fun add(entity: AdvisoryAccess): AdvisoryAccess = transaction {
        AdvisoryDao.checkIfAlreadyExist {
            (AdvisoryDao.classYear eq entity.classYear) and
                    (AdvisoryDao.advisoryName eq entity.advisoryName) and
                    (AdvisoryDao.sectionNumber eq entity.sectionNumber)
        }.run {
            if (this) throw AlreadyExistError("Advisory Already Exist", originName)
        }
        AdvisoryDao.insertOne(entity)
    }

    override fun delete(entity: AdvisoryAccess) = transaction {
        AdvisoryDao.deleteWhere { AdvisoryDao.id eq entity.id }
        commit()
    }

    override fun update(entity: AdvisoryAccess): AdvisoryAccess = transaction {
        AdvisoryDao.updateOne(entity) {
            AdvisoryDao.id eq entity.id
        }
    }

    override fun findOneById(id: Int): AdvisoryAccess? = transaction {
        AdvisoryDao.select { AdvisoryDao.id eq id }.map { it.toEntity() }.firstOrNull()
    }

    fun findOneByIdEager(id: Int): AdvisoryAccess? = transaction {
        AdvisoryDao
            .innerJoin(AdviserDao)
            .leftJoin(PersonDao, { AdviserDao.person }, { PersonDao.id })
            .select { AdvisoryDao.id eq id }
            .map { it.joinAllEager() }
            .firstOrNull()
    }

    override fun findAllAndPaginate(count: Int, offset: Int): List<AdvisoryAccess> = transaction {
        AdvisoryDao.findAllAndPaginate(count, offset)
    }

    override fun UpdateBuilder<Any>.from(entity: AdvisoryAccess, dao: AdvisoryDao) {
        this[dao.id] = entity.id
        this[dao.classYear] = entity.classYear
        this[dao.classLevel] = entity.classLevel
        this[dao.advisoryName] = entity.advisoryName
        this[dao.sectionName] = entity.sectionName
        this[dao.sectionNumber] = entity.sectionNumber
        this[dao.listStudentId] = entity.listStudentId
        this[dao.createdBy] = entity.createdBy
        this[dao.modifiedBy] = entity.modifiedBy
        this[dao.adviser] = entity.adviser?.id ?: throw InvalidRequiredInputsError(
            "Adviser not properly Initialized",
            originName
        )

    }

    fun findBySearchSCriteria(
        searchCriteria: SearchAdvisoryP
    ) = transaction {
        val query = AdvisoryDao.joinAdviserPerson().select {
            AdvisoryDao.classYear eq searchCriteria.classYear
        }

        varyQuery(query, searchCriteria)
            .mapNotNull { it.joinWithPerson() }
    }

    private fun AdvisoryDao.joinAdviserPerson() =
        this.innerJoin(AdviserDao.source)
            .leftJoin(PersonDao, { AdviserDao.person }, { PersonDao.id })
            .slice(fieldSets)

    private fun varyQuery(
        query: Query,
        searchCriteria: SearchAdvisoryP
    ): Query {
        var outputQuery = query
        outputQuery = searchCriteria.classLevel?.let {
            when (it.toUpperCase()) {
                "ALL" -> outputQuery
                else -> outputQuery.andWhere { AdvisoryDao.classLevel eq it.toInt() }
            }
        } ?: outputQuery

        outputQuery = searchCriteria.adviserId?.let {
            outputQuery.andWhere { AdvisoryDao.adviser eq it }
        } ?: outputQuery

        outputQuery = searchCriteria.classSection?.let {
            when (it.toUpperCase()) {
                "ALL" -> outputQuery
                else -> outputQuery.andWhere { AdvisoryDao.sectionNumber eq it.toInt() }
            }
        } ?: outputQuery
        return outputQuery
    }

    private fun ResultRow.joinAllEager(): AdvisoryAccess {
        val advisoryAccess = this.toEntity()
        advisoryAccess.adviser = adviserRepository.joinAdviserWithPerson(this)
        return advisoryAccess
    }

    private fun ResultRow.joinWithPerson(): AdvisoryJoinAdviserPrsn {
        val person = personRepository.resultToEntity(this)

        return AdvisoryJoinAdviserPrsn(
            this[AdvisoryDao.id],
            this[AdvisoryDao.classYear],
            this[AdvisoryDao.classLevel],
            this[AdvisoryDao.sectionName],
            this[AdvisoryDao.sectionNumber],
            this[AdvisoryDao.advisoryName],
            person.toPerson()
        )
    }

    override fun ResultRow.toEntity(): AdvisoryAccess {
        val advisoryAccess = AdvisoryAccess()
        advisoryAccess.id = this[AdvisoryDao.id]
        advisoryAccess.classYear = this[AdvisoryDao.classYear]
        advisoryAccess.classLevel = this[AdvisoryDao.classLevel]
        advisoryAccess.sectionName = this[AdvisoryDao.sectionName]
        advisoryAccess.sectionNumber = this[AdvisoryDao.sectionNumber]
        advisoryAccess.listStudentId = this[AdvisoryDao.listStudentId]
        advisoryAccess.advisoryName  = this[AdvisoryDao.advisoryName]
        advisoryAccess.adviser = AdviserAccess() itsID this[AdvisoryDao.adviser]
        advisoryAccess.listStudentAccess = listOf()

        advisoryAccess.createdAt = this[AdvisoryDao.createdAt]
        advisoryAccess.modifiedAt = this[AdvisoryDao.modifiedAt]
        return advisoryAccess
    }

    private fun ResultRow.joinWithAdviser(): AdvisoryAccess {

        val advisoryAccess = AdvisoryAccess()
        advisoryAccess.id = this[AdvisoryDao.id]
        advisoryAccess.classYear = this[AdvisoryDao.classYear]
        advisoryAccess.classLevel = this[AdvisoryDao.classLevel]
        advisoryAccess.sectionName = this[AdvisoryDao.sectionName]
        advisoryAccess.sectionNumber = this[AdvisoryDao.sectionNumber]
        advisoryAccess.listStudentId = this[AdvisoryDao.listStudentId]
        advisoryAccess.adviser = adviserRepository.joinAdviserWithPerson(this)

        return advisoryAccess
    }
}