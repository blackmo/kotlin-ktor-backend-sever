package com.vhl.sms.caster.datasource.access

import com.vhl.sms.caster.commons.model.Device
import com.vhl.sms.caster.exposed.Dao
import com.vhl.sms.caster.exposed.Domain
import java.time.LocalDateTime

object DeviceDao: Dao("device") {
    val deviceId = varchar("device_id_str", 100).uniqueIndex()
    val mobileNumber = varchar("mobile_number", 15).uniqueIndex()
    val user = varchar("user_name", 50)
}

class DeviceAccess: Domain {
    override var id = 0
    var deviceId: String = ""
    var mobileNumber: String = ""
    var user: String = ""
    override var createdAt: LocalDateTime? = null
    override var modifiedAt: LocalDateTime? = null

    infix fun from(device: Device): DeviceAccess {
        this.deviceId = device.deviceId
        this.mobileNumber = device.mobileNumber
        this.user = device.user

        return this
    }
}