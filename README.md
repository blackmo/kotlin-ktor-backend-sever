# RND Project: Sms Caster Server
A project that uses most kotlin libraries for dependency injection and uses Kotlin Coroutines

## Project Setup
clone the following repo in the within the project folder:

e.g :

root_dir/sms-caster-server/
```bash
git clone git@bitbucket.org:blackmo/sms-caster-commons.git
```

follow the README file inside the sms-caster-android-lib

## Usage
update desired port on application.conf in resources folder
```
    deployment {
        port = {port number}
        watch = [com.vhl.sms.caster]
    }
 ```

## Contributing
pull request are welcome

